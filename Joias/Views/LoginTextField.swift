//
//  LoginTextField.swift
//  Joias
//
//  Created by Gilson Gil on 4/18/16.
//  Copyright (c) 2016 doisdoissete. All rights reserved.
//

import UIKit

final class LoginTextField: UITextField {
  private let label = UILabel()
  var invalidInput = false
  
  func configureWithText(text: String) {
    label.text = text
    label.textColor = UIColor.loginTextFieldLabelColor()
    label.textAlignment = .Center
    label.font = UIFont.lightFontWithSize(16)
    
    let width = (text as NSString).boundingRectWithSize(CGSize(width: 200, height: bounds.height), options: .UsesLineFragmentOrigin, attributes: [NSFontAttributeName: label.font], context: nil).width
    let padding: CGFloat = 16
    
    leftView = UIView(frame: CGRect(origin: CGPointZero, size: CGSize(width: width + padding, height: bounds.height)))
    leftView?.addSubview(label)
    label.frame = leftView!.bounds
    leftViewMode = .Always
    
    rightView = UIView(frame: CGRect(origin: CGPointZero, size: CGSize(width: padding, height: bounds.height)))
    rightViewMode = .Always
  }
  
  func invalidate() {
    label.textColor = UIColor.loginInvalidInputColor()
    textColor = UIColor.loginInvalidInputColor()
    invalidInput = true
  }
  
  func deInvalidate() {
    if invalidInput {
      invalidInput = false
      label.textColor = UIColor.blackColor()
      textColor = UIColor.lightGrayColor()
    }
  }
}
