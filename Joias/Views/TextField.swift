//
//  TextField.swift
//  Joias
//
//  Created by Gilson Gil on 3/14/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class TextField: UITextField {
  override func awakeFromNib() {
    super.awakeFromNib()
    setPaddings()
  }
  
  func setPaddings() {
    setPaddings(8)
  }
  
  func setPaddings(padding: CGFloat) {
    leftView = UIView(frame: CGRect(origin: CGPointZero, size: CGSize(width: padding, height: bounds.height)))
    leftViewMode = .Always
    rightView = UIView(frame: CGRect(origin: CGPointZero, size: CGSize(width: padding, height: bounds.height)))
    rightViewMode = .Always
  }
}
