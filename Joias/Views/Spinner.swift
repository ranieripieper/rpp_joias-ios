//
//  Spinner.swift
//  Joias
//
//  Created by Gilson Gil on 3/14/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class Spinner: UIView {
  var isAnimating = false
  var hidesWhenStopped = true
  var fillColor = UIColor.loadingTopColor().CGColor
  var pathColor = UIColor.loadingBottomColor().CGColor
  var rotatingAngle: CGFloat = 6
  var thickness: CGFloat = 1
  
  // MARK: Init
  override init(frame: CGRect) {
    super.init(frame: frame)
    defaultInit()
  }
  
  required init(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    defaultInit()
  }
  
  func defaultInit() {
    backgroundColor = UIColor.clearColor()
    hidden = hidesWhenStopped
  }
  
  // MARK: Draw
  override func drawRect(rect: CGRect) {
    let centerPoint = CGPoint(x: bounds.width / 2, y: bounds.height / 2)
    let context = UIGraphicsGetCurrentContext()
    let arcRadius = (bounds.width - thickness) / 2
    let arc = UIBezierPath(arcCenter: centerPoint, radius: arcRadius, startAngle: radiansWithDegrees(0), endAngle: radiansWithDegrees(360), clockwise: false)
    let shape = CGPathCreateCopyByStrokingPath(arc.CGPath, nil, thickness, kCGLineCapRound, kCGLineJoinRound, 0)
    CGContextBeginPath(context)
    CGContextAddPath(context, shape)
    CGContextSetFillColorWithColor(context, pathColor)
    CGContextFillPath(context)
    var currentAngle: CGFloat = 180
    var newAlphaValue: CGFloat = 0
    let numberOfSteps = Int(360 / rotatingAngle)
    for var i = 0; i < numberOfSteps; i++ {
      newAlphaValue = newAlphaValue + CGFloat(1) / CGFloat(numberOfSteps)
      if newAlphaValue > 1 {
        newAlphaValue = 0
      }
      let arcEndAngle = currentAngle + rotatingAngle
      let arcStartAngle = currentAngle
      CGContextAddArc(context, centerPoint.x, centerPoint.y, (bounds.width - thickness) / 2, radiansWithDegrees(arcStartAngle), radiansWithDegrees(arcEndAngle), 0)
      CGContextSetLineWidth(context, thickness)
      let newColor = CGColorCreateCopyWithAlpha(fillColor, newAlphaValue)
      CGContextSetStrokeColorWithColor(context, newColor)
      if numberOfSteps - 1 == i {
        CGContextSetLineCap(context, kCGLineCapRound)
      }
      CGContextStrokePath(context)
      CGContextSetFillColorWithColor(context, newColor)
      CGContextFillPath(context)
      currentAngle = currentAngle + rotatingAngle
    }
  }
  
  func radiansWithDegrees(degrees: CGFloat) -> CGFloat {
    return degrees / 180 * CGFloat(M_PI)
  }
  
  // MARK: Animation
  func startAnimating() {
    isAnimating = true
    hidden = false
    startRotating()
  }
  
  func stopAnimating() {
    isAnimating = false
    hidden = hidesWhenStopped
    stopRotating()
    setNeedsDisplay()
  }
  
  func startRotating() {
    let rotate = CABasicAnimation(keyPath: "transform.rotation")
    rotate.byValue = M_PI * 2
    rotate.duration = 1
    rotate.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
    rotate.repeatCount = Float(INTMAX_MAX)
    layer.addAnimation(rotate, forKey: "Rotate")
  }
  
  func stopRotating() {
    layer.removeAnimationForKey("Rotate")
  }
}
