//
//  TrySelectionHeader.swift
//  Joias
//
//  Created by Gilson Gil on 3/12/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class TrySelectionHeader: UICollectionReusableView {
  @IBOutlet weak var jewelCategoryLabel: UILabel!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    jewelCategoryLabel.textColor = UIColor.tryTextColor()
    jewelCategoryLabel.font = UIFont.tryFont()
  }
  
  func configureWithJewelCategoryString(string: String) {
    jewelCategoryLabel.text = "Qual \(string) você quer experimentar?"
  }
}
