//
//  TouchableImageView.swift
//  Joias
//
//  Created by Gilson Gil on 3/12/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

protocol TouchableImageViewDelegate {
  func touchableImageViewIsHolding(isHolding: Bool)
  func touchableImageViewIsDeleted(isDeleted: Bool, andObject object: AnyObject)
}

class TouchableImageView: UIImageView {
  var isScaled = false
//  var isReproducing = false
//  var isBlinking = false
//  var blinkTimer: NSTimer?
  var startPoint = CGPointZero
  var imageName: String?
  var endPoint = CGPointZero
//  var originalFrame = CGRectZero
  var delegate: TouchableImageViewDelegate?
  var canChangeImage = false
  var isSelected = true

  var trans: CGAffineTransform?
  var insideTrans: CGAffineTransform?
  var currentScale: CGFloat = 1
  var lastScale: CGFloat = 1
  var currentAngle: CGFloat = 0
  var lastAngle: CGFloat = 0
  var insideScale = false
  var isTouching = false
  
  var insideImageView: UIImageView?
  
  override init(image: UIImage!, highlightedImage: UIImage?) {
    super.init(image: image, highlightedImage: highlightedImage)
    initializationConfig()
  }
  
  override init(image: UIImage!) {
    super.init(image: image)
    initializationConfig()
  }
  
  convenience init(imageNamed: String) {
    let image = UIImage(named: imageNamed)
    self.init(image: image)
  }

  required init(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    initializationConfig()
  }
  
  // MARK: Init
  func initializationConfig() {
    userInteractionEnabled = true
    
//    let pinch = UIPinchGestureRecognizer(target: self, action: "handlePinch:")
//    addGestureRecognizer(pinch)
    
//    let rotation = UIRotationGestureRecognizer(target: self, action: "handleRotation:")
//    addGestureRecognizer(rotation)
    
//    let twoFingerTap = UITapGestureRecognizer(target: self, action: "handleTwoFingerTap:")
//    twoFingerTap.numberOfTapsRequired = 1
//    twoFingerTap.numberOfTouchesRequired = 2
//    addGestureRecognizer(twoFingerTap)
    
//    let tap = UITapGestureRecognizer(target: self, action: "handleTap:")
//    tap.numberOfTapsRequired = 1
//    tap.numberOfTouchesRequired = 1
//    addGestureRecognizer(tap)
    
//    let doubleTap = UITapGestureRecognizer(target: self, action: "handleDoubleTap:")
//    doubleTap.numberOfTapsRequired = 2
//    doubleTap.numberOfTouchesRequired = 1
//    addGestureRecognizer(doubleTap)
//    
//    let tripleTap = UITapGestureRecognizer(target: self, action: "handleTripleTap:")
//    tripleTap.numberOfTapsRequired = 3
//    tripleTap.numberOfTouchesRequired = 1
//    addGestureRecognizer(tripleTap)
//    
//    let fourTap = UITapGestureRecognizer(target: self, action: "handleFourTap:")
//    fourTap.numberOfTapsRequired = 4
//    fourTap.numberOfTouchesRequired = 1
//    addGestureRecognizer(fourTap)
//    
//    let longPress = UILongPressGestureRecognizer(target: self, action: "handleLongPress:")
//    addGestureRecognizer(longPress)
  }
  
  // MARK: Blink
//  func startBlinking() {
//    blinkTimer = NSTimer.scheduledTimerWithTimeInterval(0.4, target: self, selector: "blinkOut", userInfo: nil, repeats: true)
//    isBlinking = true
//  }
  
//  func blinkIn() {
//    UIView.beginAnimations("blinkIn", context: nil)
//    UIView.setAnimationDuration(0.2)
//    UIView.setAnimationBeginsFromCurrentState(true)
//    alpha = 1
//    UIView.commitAnimations()
//  }
  
//  func blinkOut() {
//    UIView.beginAnimations("blinkOut", context: nil)
//    UIView.setAnimationDuration(0.2)
//    UIView.setAnimationBeginsFromCurrentState(true)
//    UIView.setAnimationDelegate(self)
//    UIView.setAnimationDidStopSelector("blinkIn")
//    alpha = 0
//    UIView.commitAnimations()
//  }
  
//  func stopBlinking() {
//    alpha = 0.5
//    blinkTimer?.invalidate()
//    isBlinking = false
//  }
  
//  func changeImageView() {
//    insideImageView = UIImageView(image: image, highlightedImage: highlightedImage)
//    addSubview(insideImageView!)
//    image = nil
//    highlightedImage = nil
//  }
  
  // MARK: Touchs Delegate
  override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
    if !canChangeImage {
      delegate?.touchableImageViewIsHolding(false)
    }
    
    if isSelected && !isTouching {
      isTouching = true
      let touch = touches.first as! UITouch
      let pt = touch.locationInView(superview)
      startPoint = convertPoint(pt, fromView: superview)
//      highlighted = true
//      alpha = 0.5
    }
  }
  
  override func touchesEnded(touches: Set<NSObject>, withEvent event: UIEvent) {
    if isSelected {
      superview?.bringSubviewToFront(self)
    }
    let touch = touches.first as! UITouch
    endPoint = touch.locationInView(window)
//    highlighted = false
    isTouching = false
//    alpha = 1
//    verifyOriginalFrame()
    if !canChangeImage {
      delegate?.touchableImageViewIsHolding(true)
    }
  }
  
  override func touchesMoved(touches: Set<NSObject>, withEvent event: UIEvent) {
    if isSelected && touches.count == 1 {
      let touch = touches.first as! UITouch
      let current = touch.locationInView(superview)
      let pt = convertPoint(current, fromView: superview)
      
      trans = transform
      insideTrans = insideImageView?.transform
      
      trans = CGAffineTransformTranslate(trans!, pt.x - startPoint.x, pt.y - startPoint.y)
      transform = trans!
      
//      if !isBlinking {
//        alpha = 0.5
//      }
    }
  }
  
  override func touchesCancelled(touches: Set<NSObject>, withEvent event: UIEvent!) {
//    verifyOriginalFrame()
    if !canChangeImage {
      delegate?.touchableImageViewIsHolding(true)
    }
//    highlighted = false
//    alpha = 1
  }
  
  func verifyOriginalFrame() {
    if !canChangeImage {
//      if frame.origin.x != originalFrame.origin.x || frame.origin.y != originalFrame.origin.y {
//        UIView.animateWithDuration(0.2) {
//          self.frame = self.originalFrame
//        }
//      }
    }
  }
  
  // MARK: Gestures
  func handlePinch(sender: UIPinchGestureRecognizer) {
//    highlighted = true
    
    if sender.state == .Began {
      trans = transform
    }
    
    lastScale = currentScale
    currentScale = sender.scale
    
    let newTransform = CGAffineTransformScale(trans!, currentScale, currentScale)
    transform = newTransform
    
    
//    if transform.a <= 0.5 {
//      trans!.a = 0.5
//      trans!.d = 0.5
//      transform = CGAffineTransformScale(trans!, currentScale, currentScale)
//    }
    
    if sender.state == .Ended {
      trans = transform
//      highlighted = false
//      alpha = 1
      isScaled = true
    }
  }
  
  func handleRotation(sender: UIRotationGestureRecognizer) {
//    if canChangeImage {
//      highlighted = true
    if trans == nil {
      trans = transform
    }
    
    currentAngle += sender.rotation - lastAngle
    lastAngle = sender.rotation
    
    let newTransform = CGAffineTransformRotate(trans!, currentAngle)
    transform = newTransform
    if sender.state == .Ended {
      trans = transform
//        highlighted = false
//        alpha = 1
//      }
    }
  }
  
//  func handleTwoFingersTap(sender: UITapGestureRecognizer) {
//    
//  }
  
//  func handleTap(sender: UITapGestureRecognizer) {
//    isSelected = !isSelected
//  }
  
//  func handleDoubleTap(sender: UITapGestureRecognizer) {
//    
//  }
//  
//  func handleTripleTap(sender: UITapGestureRecognizer) {
//    
//  }
//  
//  func handleFourTap(sender: UITapGestureRecognizer) {
//    
//  }
//  
//  func handleLongPress(sender: UITapGestureRecognizer) {
//    
//  }
  
  // MARK: Selected
//  func setSelected(selected: Bool) {
//    isSelected = selected
//  }
}
