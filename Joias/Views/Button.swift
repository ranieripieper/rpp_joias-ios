//
//  Button.swift
//  Joias
//
//  Created by Gilson Gil on 3/14/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class Button: UIButton {
  override func drawRect(rect: CGRect) {
    let context = UIGraphicsGetCurrentContext()
    CGContextSetStrokeColorWithColor(context, UIColor.whiteColor().CGColor)
    CGContextSetLineWidth(context, 1)
    CGContextMoveToPoint(context, 0, 0)
    CGContextAddLineToPoint(context, bounds.width, 0)
    CGContextAddLineToPoint(context, bounds.width, bounds.height)
    CGContextAddLineToPoint(context, 0, bounds.height)
    CGContextClosePath(context)
    CGContextStrokePath(context)
  }
}
