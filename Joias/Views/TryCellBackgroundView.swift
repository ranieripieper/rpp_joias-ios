//
//  TryCellBackgroundView.swift
//  Joias
//
//  Created by Gilson Gil on 3/12/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class TryCellBackgroundView: UIView {
  override func drawRect(rect: CGRect) {
    let context = UIGraphicsGetCurrentContext()
    CGContextSetStrokeColorWithColor(context, UIColor.lightGrayColor().CGColor)
    CGContextMoveToPoint(context, 0, 0)
    CGContextAddLineToPoint(context, bounds.width, 0)
    CGContextAddLineToPoint(context, bounds.width, bounds.height)
    CGContextAddLineToPoint(context, 0, bounds.height)
    CGContextAddLineToPoint(context, 0, 0)
    CGContextStrokePath(context)
  }
}
