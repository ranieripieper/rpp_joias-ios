//
//  HerreiraConstants.swift
//  Joias
//
//  Created by Gilson Gil on 5/10/16.
//  Copyright (c) 2016 doisdoissete. All rights reserved.
//

import Foundation

struct Constants {
  typealias Color = (red: Float, green: Float, blue: Float)
  
  static let appName = "Herreira"
  
  static let serverURL = "http://54.207.96.156:93/"
  static let serverToken = "6a6efa5f-98d0-4704-a50f-35e9b5bbdca1"
  
  static let instagramUserId = "221850913"
  static let facebookUserId = "herreirasemijoias"
  
  static let appStoreLink = "https://goo.gl/PTLI3b"
  
  static let instagramClientId = "148078b7fa5745f6bd890aa18b1f5ae9"
  static let facebookAppId = "314656991967495"
  static let facebookAppSecret = "7e0a8f56be9ad870b06db6b0267f8470"
  
  static let parseApplicationId = "ZrsbO91X4XW2VkV93U37GojYypDaWbVv6gWN5fK5"
  static let parseClientKey = "jATu5vbINdeBrbHyLm70ius5we1dm5hI6q72Bvf5"
  
  static let navigationBarBackgroundColor: Color = (red: 97 / 255, green: 101 / 255, blue: 103 / 255)
  static let navigationBarColor: Color = (red: 135 / 255, green: 190 / 255, blue: 189 / 255)
  static let collectionCategoryColor: Color = (red: 135 / 255, green: 190 / 255, blue: 189 / 255)
  static let loadingBottomColor: Color = (red: 164 / 255, green: 219 / 255, blue: 217 / 255)
  static let loadingTopColor: Color = (red: 115 / 255, green: 170 / 255, blue: 169 / 255)
  static let collectionButtonsColor: Color = (red: 135 / 255, green: 190 / 255, blue: 189 / 255)
  static let tryTextColor: Color = (red: 135 / 255, green: 190 / 255, blue: 189 / 255)
  static let tryAdjustColor: Color = (red: 97 / 255, green: 101 / 255, blue: 103 / 255)
  static let jewelCategoryColor: Color = (red: 135 / 255, green: 190 / 255, blue: 189 / 255)
  static let interestBackgroundColor: Color = (red: 135 / 255, green: 190 / 255, blue: 189 / 255)
  static let loginAlertLineColor: Color = (red: 109.0/255.0, green: 131.0/255.0, blue: 132.0/255.0)
  static let loginAlertBackgroundColor: Color = (red: 139.0/255.0, green: 167.0/255.0, blue: 167.0/255.0)
  static let loginInvalidInputColor: Color = (red: 176.0/255.0, green: 110.0/255.0, blue: 110.0/255.0)
  static let loginTextFieldLabelColor: Color = (red: 98.0/255.0, green: 101.0/255.0, blue: 103.0/255.0)
  
  static let regularFont = "Exo2-Regular"
  static let lightFont = "Exo2-Light"
  static let mediumFont = "Exo2-Medium"
  static let semiboldFont = "Exo2-SemiBold"
  static let tryFont = "Exo2-Medium"
  
  static let aboutText = "Inaugurada a 8 anos, a fábrica de semijoias finas, Herreira, surgiu do desejo de satisfazer os anseios femininos por acessórios que aliassem design e qualidade, buscando em todas as etapas do processo fabril, da criação ao acabamento, a satisfação das necessidades dos seus clientes, bem como, surpreende-los com a antecipação e conversão das principais tendências da moda internacional, em seus designs.\n\nComo principais valores, a Herreira tem a meritocracia e o compromisso com a felicidade e realização dos seus colaboradores, além do constante investimento em relações comerciais de qualidade e reciprocamente vantajosas com parceiros, revendedores e clientes finais.\n\nConheça mais e faça parte dessa história de sucesso, entrando em contato através dos telefones: (11) 2615 0978 ou (62) 3281 2225, ou ainda, através do e-mail,  contato@herreirasemijoias.com.br."

  static let earringsCount = 32
  static let ringsCount = 33
}
