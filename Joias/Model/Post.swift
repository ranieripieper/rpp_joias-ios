//
//  Post.swift
//  Joias
//
//  Created by Gilson Gil on 2/21/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import Foundation

enum SocialType {
  case Instagram, Facebook
}

final class Post: ResponseCollectionSerializable {
  let id: String
  let username: String
  let avatar: String
  let timestamp: NSTimeInterval
  let description: String
  let thumbnail: String
  var picture: String
  var likes: Int?
  var comments: Int?
  var shares: Int?
  let socialType: SocialType
  let link: String
  
  class func collection(#response: NSHTTPURLResponse, representation: AnyObject) -> [Post] {
    var posts = [Post]()
    
    if response.statusCode == 200 {
      let dateFormatter = NSDateFormatter()
      dateFormatter.dateFormat = "yyyy'-'MM'-'dd'T'HH':'mm':'ssZZZZ"
      for post in representation.valueForKeyPath("data") as! [NSDictionary] {
        let newPost = Post(JSON: post, dateFormatter: dateFormatter)
        posts.append(newPost)
        if newPost.socialType == .Facebook {
          FacebookAPI.largeImage(newPost.id) { result in
            switch result {
            case .Success(let boxedUrl):
              newPost.picture = boxedUrl.unbox
            case .Failure(let error):
              break
            }
          }
          if newPost.likes == nil {
            FacebookAPI.likes(newPost.id) { result in
              switch result {
              case .Success(let boxedCount):
                newPost.likes = boxedCount.unbox
              case .Failure(let error):
                break
              }
            }
          }
          if newPost.comments == nil {
            FacebookAPI.comments(newPost.id) { result in
              switch result {
              case .Success(let boxedCount):
                newPost.comments = boxedCount.unbox
              case .Failure(let error):
                break
              }
            }
          }
        }
      }
    }
    
    return posts
  }
  
  init(JSON: AnyObject, dateFormatter: NSDateFormatter) {
    if let username = JSON.valueForKeyPath("user.username") as? String {
      socialType = .Instagram
      self.username = username
    } else {
      socialType = .Facebook
      self.username = JSON.valueForKeyPath("from.name") as? String ?? ""
    }
    id = JSON.valueForKeyPath("object_id") as? String ?? JSON.valueForKeyPath("id") as? String ?? ""
    avatar = JSON.valueForKeyPath("user.profile_picture") as? String ?? "http://graph.facebook.com/v1.0/herreirasemijoias/picture"
    if let dateCreated = JSON.valueForKeyPath("created_time") as? String {
      if let date = dateFormatter.dateFromString(dateCreated) {
        timestamp = date.timeIntervalSince1970
      } else {
        timestamp = NSTimeInterval((JSON.valueForKeyPath("created_time") as! String).toInt() ?? Int(NSTimeIntervalSince1970))
      }
    } else {
      timestamp = NSTimeIntervalSince1970
    }
    link = JSON.valueForKeyPath("link") as? String ?? ""
    description = JSON.valueForKeyPath("message") as? String ?? JSON.valueForKeyPath("caption.text") as? String ?? ""
    thumbnail = JSON.valueForKeyPath("images.standard_resolution.url") as? String ?? JSON.valueForKeyPath("picture") as? String ?? ""
    picture = self.thumbnail
    likes = JSON.valueForKeyPath("likes.count") as? Int
    comments = JSON.valueForKeyPath("comments.count") as? Int
    shares = JSON.valueForKeyPath("shares.count") as? Int
  }
}