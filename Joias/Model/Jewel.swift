//
//  Jewel.swift
//  Joias
//
//  Created by Gilson Gil on 2/21/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import Foundation

final class Jewel {
  let name: String
  let picture: Picture
  let category: Category
  let collection: Collection
  let description: String
  
  enum Category {
    case Ring, Necklace, Earrings, Bracelet, None
    
    func toString() -> String {
      switch self {
      case .Ring:
        return "Anel"
      case .Necklace:
        return "Colar"
      case .Earrings:
        return "Brincos"
      case .Bracelet:
        return "Pulseira"
      case .None:
        return ""
      }
    }
    
    func prenoun() -> String {
      switch self {
      case .Ring, .Necklace:
        return "Este"
      case .Earrings:
        return "Estes"
      case .Bracelet:
        return "Esta"
      case .None:
        return ""
      }
    }
  }
  
  enum Collection {
    case Classic, New, None
    
    init(_ string: String) {
      switch string.lowercaseString {
      case "classic", "clássicas":
        self = .Classic
      case "new", "lançamentos":
        self = .New
      default:
        self = .None
      }
    }
    
    func toString() -> String {
      switch self {
      case .Classic:
        return "Clássicas"
      case .New:
        return "Lançamentos"
      default:
        return ""
      }
    }
  }
  
  init(name: String, picture: Picture, category: Category, collection: Collection, description: String) {
    self.name = name
    self.picture = picture
    self.category = category
    self.collection = collection
    self.description = description
  }
  
  init(JSON: NSDictionary) {
    name = JSON["nome"] as? String ?? ""
    picture = Picture(URL: JSON["imagem"] as? String ?? "", width: JSON["imagem_largura"] as? Float ?? 0, height: JSON["imagem_altura"] as? Float ?? 0)
    category = .None
    collection = Collection(JSON["colecao"] as? String ?? "Clássicas")
    description = JSON["descricao"] as? String ?? ""
  }
}

extension Jewel: ResponseCollectionSerializable {
  class func collection(#response: NSHTTPURLResponse, representation: AnyObject) -> [Jewel] {
    var jewels = [Jewel]()
    
    if response.statusCode == 200 {
      for jewel in representation as! [NSDictionary] {
        let newJewel = Jewel(JSON: jewel)
        let imageURL = newJewel.picture.URL.stringByFoldingWithOptions(.DiacriticInsensitiveSearch, locale: NSLocale.currentLocale())
        if imageURL == newJewel.picture.URL {
          jewels.append(newJewel)
        }
      }
    }
    
    return jewels
  }
}
