//
//  Video.swift
//  Joias
//
//  Created by Gilson Gil on 3/13/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

final class Video {
  var thumb: UIImage?
  let label: String
  let originalLink: String
  var playableLink: String?

  init(JSON: NSDictionary) {
    thumb = nil
    label = JSON["title"] as? String ?? ""
    originalLink = JSON["link"] as? String ?? ""
  }
}

extension Video: ResponseCollectionSerializable {
  static func collection(#response: NSHTTPURLResponse, representation: AnyObject) -> [Video] {
    var videos = [Video]()
    
    if response.statusCode == 200 {
      for video in representation as! [NSDictionary] {
        let newVideo = Video(JSON: video)
        videos.append(newVideo)
      }
    }
    
    return videos
  }
}
