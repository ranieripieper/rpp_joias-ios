//
//  Picture.swift
//  Joias
//
//  Created by Gilson Gil on 5/11/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import Foundation

class Picture {
  let URL: String
  let width: Float
  let height: Float
  
  init(URL: String, width: Float, height: Float) {
    self.URL = URL
    self.width = width
    self.height = height
  }
}
