//
//  AppDelegate.swift
//  Joias
//
//  Created by Gilson Gil on 2/21/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit
import Fabric
import Crashlytics

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
  var window: UIWindow?
  var loginWindow: UIWindow?

  func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
    ParseHelper.setApplication()
    ParseHelper.registerForRemoteNotifications()
    Defaults.remove("FacebookAccessToken")
    Defaults.synchronize()
    window?.tintColor = UIColor.navigationBarColor()
    Fabric.with([Crashlytics()])
    
    if !NSUserDefaults.standardUserDefaults().boolForKey("HasPresentedLogin") && ParseHelper.loggedInUser() == nil {
      NSUserDefaults.standardUserDefaults().setBool(true, forKey: "HasPresentedLogin")
      NSUserDefaults.standardUserDefaults().synchronize()
      goToLogin(false)
    }
    return true
  }
  
  func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
    ParseHelper.didRegisterForRemoteNotificationsWithDeviceToken(deviceToken)
  }
  
  func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject]) {
    PFPush.handlePush(userInfo)
    if application.applicationState == UIApplicationState.Inactive {
      PFAnalytics.trackAppOpenedWithRemoteNotificationPayload(userInfo)
    }
  }
  
  func goToMain() {
    window?.makeKeyAndVisible()
    window?.hidden = false
    
    UIView.animateWithDuration(0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .CurveEaseInOut, animations: { [weak self] in
      self?.loginWindow?.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, 0, self!.loginWindow!.bounds.height)
      }, completion: { [weak self] _ in
        self?.loginWindow?.resignKeyWindow()
        self?.loginWindow?.removeFromSuperview()
        self?.loginWindow = nil
      })
  }
  
  func goToLogin(animated: Bool) {
    loginWindow = UIWindow(frame: UIScreen.mainScreen().bounds)
    loginWindow?.rootViewController = UIStoryboard(name: "Login", bundle: nil).instantiateInitialViewController() as? UIViewController
    loginWindow?.windowLevel = UIWindowLevelStatusBar - 1
    loginWindow?.tintColor = UIColor.navigationBarColor()
    loginWindow?.makeKeyAndVisible()
    
    if animated {
      loginWindow?.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, 0, loginWindow!.bounds.height)
      UIView.animateWithDuration(0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .CurveEaseInOut, animations: { [weak self] in
        self?.loginWindow?.transform = CGAffineTransformIdentity
        }, completion: { [weak self] _ in
          self?.window?.resignKeyWindow()
          self?.window?.removeFromSuperview()
          self?.window?.hidden = true
      })
    } else {
      window?.resignKeyWindow()
      window?.removeFromSuperview()
      window?.hidden = true
    }
  }
}

