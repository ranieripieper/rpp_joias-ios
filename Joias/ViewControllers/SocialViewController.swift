//
//  SocialViewController.swift
//  Joias
//
//  Created by Gilson Gil on 2/21/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class SocialViewController: UIViewController {
  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var tableViewWidthConstraint: NSLayoutConstraint!
  @IBOutlet weak var spinner: Spinner!
  @IBOutlet weak var spinnerBottomSpaceConstraint: NSLayoutConstraint!
  
  var socialType: SocialType = .Instagram
  var nextPage: String?
  var loadingMedia: Bool = false {
    didSet {
      if loadingMedia {
        spinner.startAnimating()
      } else {
        spinner.stopAnimating()
      }
    }
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    tableView.registerNib(UINib(nibName: "LoadingTableViewCell", bundle: nil), forCellReuseIdentifier: "LoadingTableViewCell")
    tableView.estimatedRowHeight = 600
    tableViewWidthConstraint.constant = UIScreen.mainScreen().bounds.width
    tableView.contentInset = UIEdgeInsets(top: 64, left: 0, bottom: 80, right: 0)
    tableView.layoutIfNeeded()
    getMedia()
    navigationItem.title = socialType == .Instagram ? "Instagram" : "Facebook"
  }
  
  @IBAction func toggleMenu(sender: UIBarButtonItem) {
    (navigationController as! MainNavigationController).toggleMenu()
  }
  
  func getMedia() {
    if loadingMedia {
      return
    }
    loadingMedia = true
    switch socialType {
    case .Facebook:
      FacebookAPI.accessToken() { result in
        switch result {
        case .Success(let accessToken):
          self.getFacebookMedia()
        case .Failure(let error):
          break
        }
      }
    case .Instagram:
      getInstagramMedia()
    }
    if let socialTableViewDelegate = tableView.dataSource as? SocialTableViewDelegate {
      if socialTableViewDelegate.datasource.count == 0 {
        spinnerBottomSpaceConstraint.constant = (UIScreen.mainScreen().bounds.height - 64 + spinner.bounds.height) / 2
      } else {
        spinnerBottomSpaceConstraint.constant = 40
      }
    }
  }
  
  func getFacebookMedia() {
    FacebookAPI.mediasForURLString(nextPage, result: callback)
  }
  
  func getInstagramMedia() {
    InstagramAPI.mediasForURLString(nextPage, result: callback)
  }
  
  func callback(result: Result<([Post], String?)>) -> () {
    switch result {
    case .Success(let boxedTuple):
      let tuple = boxedTuple.unbox
      let posts = tuple.0
      self.nextPage = tuple.1
      if let socialTableViewDelegate = self.tableView.dataSource as? SocialTableViewDelegate {
        socialTableViewDelegate.datasource.addElementsOf(posts)
      }
      self.tableView.reloadData()
      loadingMedia = false
    case .Failure(let error):
      UIView.animateWithDuration(0.3) {
        self.tableView.contentInset = UIEdgeInsets(top: 64, left: 0, bottom: 0, right: 0)
      }
    }
  }
}
