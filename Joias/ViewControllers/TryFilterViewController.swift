//
//  TryFilterViewController.swift
//  
//
//  Created by Gilson Gil on 10/13/15.
//
//

import UIKit
class TryFilterViewController: UIViewController {
  @IBOutlet weak var bgView: UIView! {
    didSet {
      bgView.backgroundColor = UIColor.collectionButtonsColor()
    }
  }
  @IBOutlet weak var imageView: UIImageView? {
    didSet {
      imageView?.image = image
    }
  }
  @IBOutlet weak var collectionView: UICollectionView!
  @IBOutlet weak var tryFilterCollectionController: TryFilterCollectionController! {
    didSet {
      tryFilterCollectionController.delegate = self
    }
  }
  
  var image: UIImage? {
    didSet {
      imageView?.image = image
    }
  }
  var jewel: Jewel?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    reload()
  }
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    if let finalViewController = segue.destinationViewController as? TryFinalViewController {
      finalViewController.image = imageView?.image
      finalViewController.jewel = jewel
    }
    navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
  }
  
  @IBAction func doneTapped(sender: UIBarButtonItem) {
    performSegueWithIdentifier("SegueFinal", sender: nil)
  }
  
  func reload() {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
      if let image = self.image {
        self.tryFilterCollectionController.datasource = ImageFilter.allFilters().map {
          return ($0.filterImageThumb(image), $0.title())
        }
      }
      dispatch_async(dispatch_get_main_queue()) {
        self.collectionView.reloadData()
      }
    }
  }
}

extension TryFilterViewController: TryFilterCollectionControllerDelegate {
  func didSelectFilterAtIndex(index: Int) {
    if let image = image {
      imageView?.image = ImageFilter.allFilters()[index].filterImage(image)
    }
  }
}
