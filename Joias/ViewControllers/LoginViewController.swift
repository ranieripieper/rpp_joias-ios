//
//  LoginViewController.swift
//  Joias
//
//  Created by Gilson Gil on 4/18/16.
//  Copyright (c) 2016 doisdoissete. All rights reserved.
//

import UIKit

final class LoginViewController: UIViewController {
  @IBOutlet private weak var createAccountButton: UIButton!
  @IBOutlet private weak var emailTextField: LoginTextField!
  @IBOutlet private weak var passwordTextField: LoginTextField!
  @IBOutlet private weak var loginButton: UIButton!
  @IBOutlet private weak var forgotPasswordButton: UIButton!
  @IBOutlet private weak var createAccountHeightConstraint: NSLayoutConstraint!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    configureView()
  }
  
  private func configureView() {
    navigationItem.title = Constants.appName
    view.backgroundColor = UIColor.collectionButtonsColor()
    
    createAccountButton.layer.borderColor = UIColor.whiteColor().CGColor
    loginButton.layer.borderColor = UIColor.whiteColor().CGColor
    let attributeString = NSMutableAttributedString(string: forgotPasswordButton.titleLabel?.text ?? "")
    attributeString.addAttribute(NSUnderlineStyleAttributeName, value: 1, range: NSMakeRange(0, attributeString.length))
    attributeString.addAttribute(NSFontAttributeName, value: UIFont.regularFontWithSize(13), range: NSMakeRange(0, attributeString.length))
    forgotPasswordButton.setAttributedTitle(attributeString, forState: .Normal)
    forgotPasswordButton.tintColor = UIColor.whiteColor()
    
    emailTextField.configureWithText("Email")
    passwordTextField.configureWithText("Senha")
  }
  
  private func login() {
    if count(emailTextField.text) == 0 {
      alertWithText("Aparentemente você não inseriu um email válido.\nPor favor, tente novamente.")
      emailTextField.invalidate()
      return
    }
    if count(passwordTextField.text) == 0 {
      alertWithText("Por favor, insira uma senha.")
      passwordTextField.invalidate()
      return
    }
    ParseHelper.login(emailTextField.text, password: passwordTextField.text) { [weak self] success, errorString in
      if success, let appDelegate = UIApplication.sharedApplication().delegate as? AppDelegate {
        appDelegate.goToMain()
      } else if let errorString = errorString {
        self?.alertWithText(errorString)
      }
    }
  }
  
  private func alertWithText(text: String) {
    performSegueWithIdentifier("SegueAlert", sender: text)
  }
}

extension LoginViewController {
  @IBAction func createNewAccountButtonTapped(sender: UIButton) {
    performSegueWithIdentifier("SegueSignup", sender: nil)
  }
  
  @IBAction func forgotPasswordTapped(sender: UIButton) {
    if count(emailTextField.text) == 0 {
      alertWithText("Aparentemente você não inseriu um email válido.\nPor favor, tente novamente.")
      emailTextField.invalidate()
      return
    }
    ParseHelper.forgotPassword(emailTextField.text) { [weak self] success, errorString in
      if success {
        self?.alertWithText("Por favor, verifique seu email.\npara alterar sua senha.")
      } else if let errorString = errorString {
        self?.alertWithText(errorString)
      } else {
        self?.alertWithText("Sentimos muito, mas algo deu errado.\nPor favor, tente novamente.")
      }
    }
  }
  
  @IBAction func loginButtonTapped(sender: UIButton) {
    login()
  }
  
  @IBAction func skipButtonTapped(sender: UIButton) {
    if let appDelegate = UIApplication.sharedApplication().delegate as? AppDelegate {
      appDelegate.goToMain()
    }
  }
}

extension LoginViewController {
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
    if let loginAlertViewController = segue.destinationViewController as? LoginAlertViewController {
      if let text = sender as? String {
        loginAlertViewController.configureWithText(text)
      }
    }
  }
}

extension LoginViewController: UITextFieldDelegate {
  func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
    if let textField = textField as? LoginTextField {
      textField.deInvalidate()
    }
    
    return true
  }
  
  func textFieldShouldReturn(textField: UITextField) -> Bool {
    switch textField {
    case emailTextField:
      passwordTextField.becomeFirstResponder()
    case passwordTextField:
      passwordTextField.resignFirstResponder()
      login()
    default:
      break
    }
    
    return true
  }
}
