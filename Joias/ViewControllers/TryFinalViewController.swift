//
//  TryFinalViewController.swift
//  Joias
//
//  Created by Gilson Gil on 3/14/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class TryFinalViewController: UIViewController {
  @IBOutlet weak var imageView: UIImageView!
  @IBOutlet weak var bgView: UIView!
  @IBOutlet weak var interestButton: UIButton!
  @IBOutlet weak var saveButton: UIButton!
  @IBOutlet weak var bgViewLeftConstraint: NSLayoutConstraint!
  @IBOutlet weak var bgViewRightConstraint: NSLayoutConstraint!
  
  var jewel: Jewel?
  var image: UIImage?
  var alertView: AlertController?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    bgView.backgroundColor = UIColor.collectionButtonsColor()
    imageView.image = image
    let interestAttributedTitle = NSAttributedString(string: "Tenho interesse", attributes: [NSForegroundColorAttributeName: UIColor.collectionButtonsColor(), NSFontAttributeName: UIFont.lightFontWithSize(22)])
    interestButton.setAttributedTitle(interestAttributedTitle, forState: .Normal)
    let saveAttributedTitle = NSAttributedString(string: "Salvar", attributes: [NSForegroundColorAttributeName: UIColor.collectionButtonsColor(), NSFontAttributeName: UIFont.lightFontWithSize(22)])
    saveButton.setAttributedTitle(saveAttributedTitle, forState: .Normal)
    if UIScreen.mainScreen().bounds.height == 480 {
      bgViewLeftConstraint.constant = 40
      bgViewRightConstraint.constant = 40
    }
  }
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
    if let interestViewController = segue.destinationViewController as? InterestViewController {
      interestViewController.jewel = jewel
    }
  }
  
  @IBAction func save(sender: UIButton) {
    UIImageWriteToSavedPhotosAlbum(image, self, "image:didFinishSavingWithError:contextInfo:", nil)
  }
  
  func image(image: UIImage, didFinishSavingWithError error: NSError?, contextInfo: UnsafeMutablePointer<Void>) {
    if error != nil {
      alertView = AlertController(title: "", message: error!.localizedDescription, buttons: nil, cancelButton: ("Ok", {
        self.alertView = nil
      }), style: .Alert)
      alertView!.alertInViewController(self)
    } else {
      alertView = AlertController(title: "Sucesso!", message: "Sua imagem foi salva na biblioteca do iPhone", buttons: nil, cancelButton: ("Ok", {
        self.alertView = nil
      }), style: .Alert)
      alertView!.alertInViewController(self)
    }
  }
  
  @IBAction func share(sender: UIButton) {
    Share.photo(image!, jewel: jewel!, inViewController: self)
  }
}
