//
//  LooksPressViewController.swift
//  Joias
//
//  Created by Gilson Gil on 2/21/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class LooksPressViewController: UIViewController {
  @IBOutlet weak var collectionView: UICollectionView!
  @IBOutlet weak var looksPressCollectionViewDelegate: LooksPressCollectionViewDelegate!
  @IBOutlet weak var spinner: Spinner!
  
  var endReached = false
  var currentPage = 0
  var loading: Bool = false {
    didSet {
      if loading {
        spinner.startAnimating()
      } else {
        spinner.stopAnimating()
      }
    }
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    collectionView.collectionViewLayout = looksPressCollectionViewDelegate.stackedGridLayout!
    collectionView.contentInset = UIEdgeInsets(top: 64, left: 0, bottom: 0, right: 0)
    collectionView.scrollIndicatorInsets = UIEdgeInsets(top: 64, left: 0, bottom: 0, right: 0)
    getLooksPress()
  }
  
  @IBAction func toggleMenu(sender: UIBarButtonItem) {
    (navigationController as! MainNavigationController).toggleMenu()
  }
  
  func getLooksPress() {
    if loading || endReached {
      return
    }
    loading = true
    API.looksPressItems(self.currentPage + 1, result: callback)
  }
  
  func callback(result: Result<[Jewel]>) -> () {
    switch result {
    case .Success(let boxed):
      if boxed.unbox.count == 0 {
        self.endReached = true
        break
      }
      if self.currentPage == 0 {
        looksPressCollectionViewDelegate.datasource = []
      }
      looksPressCollectionViewDelegate.datasource += boxed.unbox
      collectionView.reloadData()
      self.currentPage++
    case .Failure(let error):
      UIView.animateWithDuration(0.3) {
        self.collectionView.contentInset = UIEdgeInsets(top: 64, left: 0, bottom: 0, right: 0)
      }
    }
    loading = false
  }
}
