//
//  JewelDetailViewController.swift
//  Joias
//
//  Created by Gilson Gil on 2/21/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class JewelDetailViewController: UIViewController {
  @IBOutlet weak var scrollView: UIScrollView!
  @IBOutlet weak var imageView: UIImageView!
  @IBOutlet weak var titleLabel: UILabel!
  @IBOutlet weak var collectionLabel: UILabel!
  @IBOutlet weak var descriptionLabel: UILabel!
  @IBOutlet weak var interestButton: UIButton!
  @IBOutlet weak var imageViewWidthConstraint: NSLayoutConstraint!
  @IBOutlet weak var imageViewHeightConstraint: NSLayoutConstraint!
  @IBOutlet weak var interestButtonTopConstraint: NSLayoutConstraint!
  
  var jewel: Jewel?
  var imageRequest: Request?
  let imageCache = NSCache()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    configureView()
    configureWithJewel(jewel!)
  }
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
    if let interestViewController = segue.destinationViewController as? InterestViewController {
      interestViewController.jewel = jewel
    }
  }
  
  func configureView() {
    imageViewWidthConstraint.constant = UIScreen.mainScreen().bounds.width
    interestButtonTopConstraint.constant = UIScreen.mainScreen().bounds.height - 40 - 50 - 64
    collectionLabel.textColor = UIColor.jewelCategoryColor()
    titleLabel.font = UIFont.lightFontWithSize(20)
    collectionLabel.font = UIFont.lightFontWithSize(16)
    descriptionLabel.font = UIFont.lightFontWithSize(12)
    let interestAttributedTitle = NSAttributedString(string: "Tenho interesse", attributes: [NSForegroundColorAttributeName: UIColor.collectionButtonsColor(), NSFontAttributeName: UIFont.lightFontWithSize(22)])
    interestButton.setAttributedTitle(interestAttributedTitle, forState: .Normal)
  }
  
  func configureWithJewel(jewel: Jewel) {
    navigationItem.title = jewel.collection.toString()
    titleLabel.text = jewel.name.uppercaseString
    collectionLabel.text = jewel.collection.toString()
    descriptionLabel.text = jewel.description
    
    if imageRequest?.request.URLString != jewel.picture.URL {
      imageRequest?.cancel()
    }
    
    if let image = imageCache.objectForKey(jewel.picture) as? UIImage {
      imageView.image = image
      imageViewHeightConstraint.constant = UIScreen.mainScreen().bounds.width * image.size.height / image.size.width
    } else {
      imageView.image = nil
      
      imageRequest = request(.GET, jewel.picture.URL.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)!)
        .validate(contentType: ["image/*"])
        .responseImage() { req, _, image, error in
          if error == nil && image != nil {
            self.imageCache.setObject(image!, forKey: req.URLString)
            if req.URLString == self.imageRequest?.request.URLString {
              self.imageView.image = image
              self.imageViewHeightConstraint.constant = UIScreen.mainScreen().bounds.width * image!.size.height / image!.size.width
            }
          }
      }
    }
  }
}
