//
//  VideosViewController.swift
//  Joias
//
//  Created by Gilson Gil on 2/21/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit
import MediaPlayer

class VideosViewController: UIViewController {
  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var videosTableViewDelegate: VideosTableViewDelegate!
  @IBOutlet weak var spinner: Spinner!
  
  let vimeoHelper = VimeoHelper()
  var playing = false
  var currentPage = 0
  var endReached = false
  var loading: Bool = false {
    didSet {
      if loading {
        spinner.startAnimating()
      } else {
        spinner.stopAnimating()
      }
    }
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    tableView.contentInset = UIEdgeInsets(top: 64, left: 0, bottom: 0, right: 0)
    tableView.scrollIndicatorInsets = UIEdgeInsets(top: 64, left: 0, bottom: 0, right: 0)
    getVideos()
  }
  
  func getVideos() {
    if loading || endReached {
      return
    }
    loading = true
    API.videoItems(self.currentPage + 1, result: callback)
  }
  
  func callback(result: Result<[Video]>) -> () {
    switch result {
    case .Success(let boxed):
      if self.currentPage == 0 {
        videosTableViewDelegate.datasource = []
      }
      if boxed.unbox.count == 0 {
        self.endReached = true
        break
      }
      videosTableViewDelegate.datasource += boxed.unbox
      tableView.reloadData()
      self.currentPage++
    case .Failure(let error):
      UIView.animateWithDuration(0.3) {
        self.tableView.contentInset = UIEdgeInsets(top: 64, left: 0, bottom: 0, right: 0)
      }
    }
    loading = false
  }
  
  // MARK: Menu
  @IBAction func toggleMenu(sender: UIBarButtonItem) {
    (navigationController as! MainNavigationController).toggleMenu()
  }
  
  func playVideo(video: Video) {
    playLink(video.originalLink)
  }
  
  func playLink(link: String) {
    if playing {
      return
    }
    playing = true
    let player = MPMoviePlayerViewController(contentURL: NSURL(string: link))
    presentViewController(player, animated: true, completion: nil)
    NSNotificationCenter.defaultCenter().addObserver(self, selector: "didFinish:", name: MPMoviePlayerPlaybackDidFinishNotification, object: nil)
  }

  func didFinish(notification: NSNotification) {
    if let reason = notification.userInfo?[MPMoviePlayerPlaybackDidFinishReasonUserInfoKey] as? Int {
//      println("reason: \(reason)")
    }
    playing = false
  }
}

extension VideosViewController: VideoCellDelegate {
  func playVideoAtCell(videoCell: VideoCell) {
    if let indexPath = tableView.indexPathForCell(videoCell) {
      let video = videosTableViewDelegate.datasource[indexPath.row]
      playVideo(video)
    }
  }
}
