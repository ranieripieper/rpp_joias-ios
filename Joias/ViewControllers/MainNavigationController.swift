//
//  MainNavigationController.swift
//  Joias
//
//  Created by Gilson Gil on 2/21/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class MainNavigationController: UINavigationController {
  var menuViewController: MenuViewController?
  var homeViewController: HomeViewController!
  lazy var collectionsViewController: CollectionsViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("CollectionsViewController") as! CollectionsViewController
  lazy var looksPressViewController: LooksPressViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("LooksPressViewController") as! LooksPressViewController
  lazy var tryMenuViewController: TryMenuViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("TryMenuViewController") as! TryMenuViewController
  lazy var videosViewController: VideosViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("VideosViewController") as! VideosViewController
  lazy var facebookViewController: SocialViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("SocialViewController") as! SocialViewController
  lazy var instagramViewController: SocialViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("SocialViewController") as! SocialViewController
  lazy var signupViewController: SignupViewController = UIStoryboard(name: "Login", bundle: nil).instantiateViewControllerWithIdentifier("SignupViewController") as! SignupViewController
  lazy var aboutViewController: AboutViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("AboutViewController") as! AboutViewController
  var currentlySelectedIndex = 0
  var blurView: UIVisualEffectView?
  var toolbarView: UIToolbar?
  
  override func preferredStatusBarStyle() -> UIStatusBarStyle {
    
    return .LightContent
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    configureView()
  }
  
  func configureView() {
    homeViewController = viewControllers.first as! HomeViewController
    navigationBar.barTintColor = UIColor.navigationBarBackgroundColor()
    navigationBar.tintColor = UIColor.navigationBarColor()
    navigationBar.titleTextAttributes = [NSFontAttributeName: UIFont.regularFontWithSize(22), NSForegroundColorAttributeName: UIColor.navigationBarColor()]
  }
  
  func toggleMenu() {
    if menuViewController != nil {
      UIView.animateWithDuration(0.3, animations: {
        let height = self.view.bounds.height - 64
        self.blurView?.frame = CGRect(x: 0, y: 64 - height, width: self.view.bounds.width, height: height)
      }) { finished in
        self.menuViewController?.view.removeFromSuperview()
        self.menuViewController?.removeFromParentViewController()
        self.menuViewController = nil
        self.blurView?.removeFromSuperview()
        self.blurView = nil
        self.toolbarView?.removeFromSuperview()
        self.toolbarView = nil
      }
    } else {
      menuViewController = storyboard!.instantiateViewControllerWithIdentifier("MenuViewController") as? MenuViewController
      menuViewController!.view.backgroundColor = UIColor(white: 1, alpha: 0.9)
      menuViewController!.mainNavigationController = self
      menuViewController!.willMoveToParentViewController(self)
      let height = view.bounds.height - 64
      let menuFrame = CGRect(x: 0, y: 64 - height, width: view.bounds.width, height: height)
      
      let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.ExtraLight)
      blurView = UIVisualEffectView(effect: blurEffect)
      blurView!.frame = menuFrame
      blurView!.setTranslatesAutoresizingMaskIntoConstraints(false)
      menuViewController!.view.frame = blurView!.bounds
      blurView!.addSubview(menuViewController!.view)
      view.insertSubview(blurView!, belowSubview: navigationBar)
      menuViewController!.didMoveToParentViewController(self)
      menuViewController!.preselectIndex(currentlySelectedIndex)
      UIView.animateWithDuration(0.3) {
        self.blurView!.frame = CGRect(x: 0, y: 64, width: self.view.bounds.width, height: height)
      }
    }
  }
  
  func selectViewController(viewController: UIViewController) {
    viewControllers = [viewController]
    toggleMenu()
  }
  
  func goToHome() {
    selectViewController(homeViewController)
    currentlySelectedIndex = 0
  }
  
  func goToCollections() {
    selectViewController(collectionsViewController)
    currentlySelectedIndex = 1
  }
  
  func goToLooks() {
    selectViewController(looksPressViewController)
    currentlySelectedIndex = 2
  }
  
  func goToTry() {
    selectViewController(tryMenuViewController)
    currentlySelectedIndex = 3
  }
  
  func goToVideos() {
    selectViewController(videosViewController)
    currentlySelectedIndex = 4
  }
  
  func goToFacebook() {
    facebookViewController.socialType = .Facebook
    selectViewController(facebookViewController)
    currentlySelectedIndex = 5
  }
  
  func goToInstagram() {
    instagramViewController.socialType = .Instagram
    selectViewController(instagramViewController)
    currentlySelectedIndex = 6
  }
  
  func goToMyProfile() {
    selectViewController(signupViewController)
    currentlySelectedIndex = 7
  }
  
  func goToAbout() {
    selectViewController(aboutViewController)
    if ParseHelper.loggedInUser() == nil {
      currentlySelectedIndex = 7
    } else {
      currentlySelectedIndex = 8
    }
  }
  
  func logout() {
    ParseHelper.logout()
    if currentlySelectedIndex == 7 {
      goToHome()
    } else {
      toggleMenu()
    }
  }
  
  func login() {
    toggleMenu()
    if let appDelegate = UIApplication.sharedApplication().delegate as? AppDelegate {
      appDelegate.goToLogin(true)
    }
  }
}
