//
//  InterestViewController.swift
//  Joias
//
//  Created by Gilson Gil on 2/21/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class InterestViewController: UIViewController {
  @IBOutlet weak var scrollView: UIScrollView!
  @IBOutlet weak var bgView: UIView!
  @IBOutlet weak var textLabel: UILabel!
  @IBOutlet weak var nameTextField: TextField!
  @IBOutlet weak var cityTextField: TextField!
  @IBOutlet weak var stateTextField: TextField!
  @IBOutlet weak var phoneTextField: TextField!
  @IBOutlet weak var emailTextField: TextField!
  @IBOutlet weak var sendButton: UIButton!
  @IBOutlet weak var cancelButton: UIButton!
  @IBOutlet weak var tap: UITapGestureRecognizer!
  @IBOutlet weak var nameTextFieldWidthConstraint: NSLayoutConstraint!
  
  var keyboardNotificationObserver: KeyboardNotificationObserver!
  var jewel: Jewel?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    configureView()
  }
  
  func configureView() {
    nameTextFieldWidthConstraint.constant = UIScreen.mainScreen().bounds.width - 24 * 2
    view.addGestureRecognizer(tap)
    bgView.backgroundColor = UIColor.interestBackgroundColor()
    let sendAttributedTitle = NSAttributedString(string: "Enviar", attributes: [NSForegroundColorAttributeName: UIColor.whiteColor(), NSFontAttributeName: UIFont.lightFontWithSize(22)])
    sendButton.setAttributedTitle(sendAttributedTitle, forState: .Normal)
    let cancelAttributedTitle = NSAttributedString(string: "Cancelar", attributes: [NSForegroundColorAttributeName: UIColor.whiteColor(), NSFontAttributeName: UIFont.lightFontWithSize(22)])
    cancelButton.setAttributedTitle(cancelAttributedTitle, forState: .Normal)
    textLabel.font = UIFont.mediumFontWithSize(17)
    nameTextField.font = UIFont.lightFontWithSize(15)
    cityTextField.font = UIFont.lightFontWithSize(15)
    stateTextField.font = UIFont.lightFontWithSize(15)
    phoneTextField.font = UIFont.lightFontWithSize(15)
    emailTextField.font = UIFont.lightFontWithSize(15)
    keyboardNotificationObserver = KeyboardNotificationObserver(scrollView: scrollView)
    keyboardNotificationObserver.installNotifications()
  }
  
  @IBAction func didTap(sender: UITapGestureRecognizer) {
    nameTextField.resignFirstResponder() || cityTextField.resignFirstResponder() || stateTextField.resignFirstResponder() || phoneTextField.resignFirstResponder() || emailTextField.resignFirstResponder()
  }
  
  @IBAction func cancel(sender: UIButton) {
    dismiss()
  }
  
  func dismiss() {
    dismissViewControllerAnimated(true, completion: nil)
  }
  
  @IBAction func send(sender: UIButton) {
    send()
  }
  
  func send() {
    let validated = validateForm()
    if validated.validated {
      sendForm()
    } else {
      showMandatoryForms(validated.invalidForms)
    }
  }
  
  func sendForm() {
    API.sendEmail(nameTextField.text, email: emailTextField.text, phone: phoneTextField.text, city: cityTextField.text, state: stateTextField.text, jewelName: jewel?.name ?? "", jewelURL: jewel?.picture.URL ?? "") {
      switch $0 {
      case .Success:
        self.successfullySent()
      case .Failure(let error):
        break
      }
    }
  }
  
  func validateForm() -> (validated: Bool, invalidForms: [Int]) {
    var missing = [Int]()
    if count(nameTextField.text) == 0 {
      missing.append(1)
    }
    if count(phoneTextField.text) == 0 {
      missing.append(2)
    }
    if count(emailTextField.text) == 0 {
      missing.append(3)
    }
    return (missing.count == 0, missing)
  }
  
  func showMandatoryForms(forms: [Int]) {
    for index in forms {
      switch index {
      case 1:
        nameTextField.showMandatory(true)
      case 2:
        phoneTextField.showMandatory(true)
      case 3:
        emailTextField.showMandatory(true)
      default:
        break
      }
    }
  }
  
  func successfullySent() {
    UIView.animateWithDuration(0.3, animations: {
      self.nameTextField.alpha = 0
      self.cityTextField.alpha = 0
      self.stateTextField.alpha = 0
      self.phoneTextField.alpha = 0
      self.emailTextField.alpha = 0
      self.sendButton.alpha = 0
      self.cancelButton.alpha = 0
      self.textLabel.alpha = 0
    }) { finished in
      self.nameTextField.text = nil
      self.cityTextField.text = nil
      self.stateTextField.text = nil
      self.phoneTextField.text = nil
      self.emailTextField.text = nil
      self.textLabel.alpha = 1
      self.textLabel.frame = CGRect(origin: self.textLabel.frame.origin, size: CGSize(width: self.textLabel.bounds.width, height: self.view.bounds.height - 74 - self.textLabel.frame.origin.y - 20))
      self.textLabel.text = "Obrigado.\nEm breve entraremos em contato."
      dispatch_after(dispatch_time(DISPATCH_TIME_NOW, Int64(3 * Double(NSEC_PER_SEC))), dispatch_get_main_queue()) {
        self.dismiss()
      }
    }
  }
}

extension InterestViewController: UITextFieldDelegate {
  func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
    return textField.tag != 3 || count((textField.text as NSString).stringByReplacingCharactersInRange(range, withString: string)) <= 2
  }
  
  func textFieldShouldReturn(textField: UITextField) -> Bool {
    if textField.tag == 5 {
      textField.resignFirstResponder()
      send()
    } else {
      if let nextTextField = view.viewWithTag(textField.tag + 1) as? UITextField {
        nextTextField.becomeFirstResponder()
      }
    }
    return true
  }
}

extension TextField {
  func showMandatory(on: Bool) {
    attributedPlaceholder = NSAttributedString(string: placeholder ?? "", attributes: [NSForegroundColorAttributeName: UIColor(red: 184/255, green: 100/255, blue: 100/255, alpha: 1)])
  }
}
