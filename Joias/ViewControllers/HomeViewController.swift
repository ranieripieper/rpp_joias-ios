//
//  HomeViewController.swift
//  Joias
//
//  Created by Gilson Gil on 2/21/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    navigationItem.title = Constants.appName
  }
  
  override func viewWillDisappear(animated: Bool) {
    super.viewWillDisappear(animated)
    navigationItem.title = ""
  }
  
  // MARK: Menu
  @IBAction func toggleMenu(sender: UIBarButtonItem) {
    (navigationController as! MainNavigationController).toggleMenu()
  }
}
