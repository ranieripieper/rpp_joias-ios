//
//  TryMenuViewController.swift
//  Joias
//
//  Created by Gilson Gil on 2/21/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class TryMenuViewController: UIViewController {
  @IBOutlet weak var earringsButton: UIButton!
//  @IBOutlet weak var necklacesButton: UIButton!
  @IBOutlet weak var ringsButton: UIButton!
//  @IBOutlet weak var braceletsButton: UIButton!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    let earringsAttributedTitle = NSAttributedString(string: "Brincos", attributes: [NSForegroundColorAttributeName: UIColor.collectionButtonsColor(), NSFontAttributeName: UIFont.lightFontWithSize(22)])
    earringsButton.setAttributedTitle(earringsAttributedTitle, forState: .Normal)
//    let necklacesAttributedTitle = NSAttributedString(string: "Colares", attributes: [NSForegroundColorAttributeName: UIColor.collectionButtonsColor(), NSFontAttributeName: UIFont.lightFontWithSize(22)])
//    necklacesButton.setAttributedTitle(necklacesAttributedTitle, forState: .Normal)
    let ringsAttributedTitle = NSAttributedString(string: "Anéis", attributes: [NSForegroundColorAttributeName: UIColor.collectionButtonsColor(), NSFontAttributeName: UIFont.lightFontWithSize(22)])
    ringsButton.setAttributedTitle(ringsAttributedTitle, forState: .Normal)
//    let braceletsAttributedTitle = NSAttributedString(string: "Pulseiras", attributes: [NSForegroundColorAttributeName: UIColor.collectionButtonsColor(), NSFontAttributeName: UIFont.lightFontWithSize(22)])
//    braceletsButton.setAttributedTitle(braceletsAttributedTitle, forState: .Normal)
  }
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
    if let trySelectionViewController = segue.destinationViewController as? TrySelectionViewController {
      if segue.identifier?.rangeOfString("SegueEarrings", options: .CaseInsensitiveSearch) != nil {
        trySelectionViewController.jewelCategory = .Earrings
      } else if segue.identifier?.rangeOfString("SegueNecklaces", options: .CaseInsensitiveSearch) != nil {
        trySelectionViewController.jewelCategory = .Necklace
      } else if segue.identifier?.rangeOfString("SegueRings", options: .CaseInsensitiveSearch) != nil {
        trySelectionViewController.jewelCategory = .Ring
      } else if segue.identifier?.rangeOfString("SegueBracelets", options: .CaseInsensitiveSearch) != nil {
        trySelectionViewController.jewelCategory = .Bracelet
      }
    }
  }
  
  @IBAction func toggleMenu(sender: UIBarButtonItem) {
    (navigationController as! MainNavigationController).toggleMenu()
  }
  
  @IBAction func earringsTapped(sender: UIButton) {
    performSegueWithIdentifier("SegueEarrings", sender: nil)
  }
  
  @IBAction func ringsTapped(sender: UIButton) {
    performSegueWithIdentifier("SegueRings", sender: nil)
  }
}
