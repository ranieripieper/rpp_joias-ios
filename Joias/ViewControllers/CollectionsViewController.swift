//
//  CollectionsViewController.swift
//  Joias
//
//  Created by Gilson Gil on 2/21/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class CollectionsViewController: UIViewController {
  @IBOutlet weak var classicButton: UIButton!
  @IBOutlet weak var newButton: UIButton!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    let classicAttributedTitle = NSAttributedString(string: "Clássicas", attributes: [NSForegroundColorAttributeName: UIColor.collectionButtonsColor(), NSFontAttributeName: UIFont.lightFontWithSize(22)])
    classicButton.setAttributedTitle(classicAttributedTitle, forState: .Normal)
    let newAttributedTitle = NSAttributedString(string: "Lançamentos", attributes: [NSForegroundColorAttributeName: UIColor.collectionButtonsColor(), NSFontAttributeName: UIFont.lightFontWithSize(22)])
    newButton.setAttributedTitle(newAttributedTitle, forState: .Normal)
  }
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
    if let collectionViewController = segue.destinationViewController as? CollectionViewController {
      if let collection = sender as? String {
        collectionViewController.collection = Jewel.Collection(collection)
      }
    }
  }
  
  @IBAction func toggleMenu(sender: UIBarButtonItem) {
    (navigationController as! MainNavigationController).toggleMenu()
  }
  
  @IBAction func classicTapped(sender: UIButton) {
    performSegueWithIdentifier("SegueCollection", sender: "classic")
  }
  
  @IBAction func newTapped(sender: UIButton) {
    performSegueWithIdentifier("SegueCollection", sender: "new")
  }
}
