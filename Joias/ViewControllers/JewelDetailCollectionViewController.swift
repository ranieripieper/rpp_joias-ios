//
//  JewelDetailCollectionViewController.swift
//  Joias
//
//  Created by Gilson Gil on 7/22/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class JewelDetailCollectionViewController: UIViewController {
  @IBOutlet weak var collectionView: UICollectionView!
  @IBOutlet weak var jewelDetailCollectionDelegate: JewelDetailCollectionDelegate!
  
  var jewelDetailViewControllers = [JewelDetailViewController?]()
  var jewels = [Jewel]()
  var currentIndex = 0
  var initialLoad = true
  
  override func viewDidLoad() {
    super.viewDidLoad()
    jewelDetailViewControllers = jewels.map { _ in
      nil
    }
    collectionView.reloadData()
  }
  
  override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()
    if initialLoad {
      collectionView.contentOffset = CGPoint(x: UIScreen.mainScreen().bounds.width * CGFloat(currentIndex), y: -64)
      initialLoad = false
    }
  }
  
  @IBAction func share(sender: UIButton) {
    if currentIndex < jewels.count, let jewelDetailViewController = jewelDetailViewControllers[currentIndex] {
      let jewel = jewels[currentIndex]
      Share.jewel(jewel, image: jewelDetailViewController.imageView.image, inViewController: self)
    }
  }
}
