//
//  SignupViewController.swift
//  Joias
//
//  Created by Gilson Gil on 4/18/16.
//  Copyright (c) 2016 doisdoissete. All rights reserved.
//

import UIKit

final class SignupViewController: UIViewController {
  @IBOutlet private weak var nameTextField: LoginTextField!
  @IBOutlet private weak var emailTextField: LoginTextField!
  @IBOutlet private weak var passwordTextField: LoginTextField!
  @IBOutlet private weak var passwordConfirmationTextField: LoginTextField!
  @IBOutlet private weak var phoneTextField: LoginTextField!
  @IBOutlet private weak var saveButton: UIButton!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    configureView()
    
    if ParseHelper.loggedInUser() != nil {
      navigationItem.leftBarButtonItems = [UIBarButtonItem(image: UIImage(named: "icn_menu"), landscapeImagePhone: nil, style: .Plain, target: self, action: "toggleMenu:")]
    }
  }
  
  override func touchesEnded(touches: Set<NSObject>, withEvent event: UIEvent) {
    super.touchesEnded(touches, withEvent: event)
    view.endEditing(true)
  }
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    if let loginAlertViewController = segue.destinationViewController as? LoginAlertViewController {
      if let text = sender as? String {
        loginAlertViewController.configureWithText(text)
      }
    }
  }
  
  private func configureView() {
    navigationItem.title = Constants.appName
    view.backgroundColor = UIColor.collectionButtonsColor()
    
    saveButton.layer.borderColor = UIColor.whiteColor().CGColor
    
    nameTextField.configureWithText("Nome")
    emailTextField.configureWithText("Email")
    passwordTextField.configureWithText("Senha")
    passwordConfirmationTextField.configureWithText("Confirmar Senha")
    phoneTextField.configureWithText("Número de Telefone")
    
    if let user = ParseHelper.loggedInUser() {
      nameTextField.text = user["name"] as? String
      emailTextField.text = user.email
      phoneTextField.text = user["phone"] as? String
      saveButton.setTitle("Salvar alterações", forState: .Normal)
    }
  }
  
  private func save() {
    view.endEditing(true)
    if count(emailTextField.text) == 0 {
      alertWithText("Aparentemente você não inseriu um email válido.\nPor favor, tente novamente.")
      emailTextField.invalidate()
      return
    }
    let update = ParseHelper.loggedInUser() != nil
    if !update && count(passwordTextField.text) == 0 {
      alertWithText("Por favor, insira uma senha.")
      passwordTextField.invalidate()
      return
    }
    if passwordTextField.text != passwordConfirmationTextField.text {
      alertWithText("As senhas diferem.\nPor favor, tente novamente.")
      passwordTextField.invalidate()
      passwordConfirmationTextField.invalidate()
      return
    }
    ParseHelper.save(nameTextField.text, email: emailTextField.text, password: passwordTextField.text, phone: phoneTextField.text) { [weak self] success, errorString in
      if success {
        if update {
          self?.alertWithText("Alterações salvas com sucesso!")
        } else if let appDelegate = UIApplication.sharedApplication().delegate as? AppDelegate {
          appDelegate.goToMain()
        }
      } else if let errorString = errorString {
        self?.alertWithText(errorString)
      }
    }
  }
  
  private func alertWithText(text: String) {
    performSegueWithIdentifier("SegueAlert", sender: text)
  }
}

extension SignupViewController {
  @IBAction func saveButtonTapped(sender: UIButton) {
    save()
  }
  
  func toggleMenu(sender: UIBarButtonItem) {
    (navigationController as! MainNavigationController).toggleMenu()
  }
}

extension SignupViewController: UITextFieldDelegate {
  func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
    if let textField = textField as? LoginTextField {
      textField.deInvalidate()
    }
    
    return true
  }
  
  func textFieldShouldReturn(textField: UITextField) -> Bool {
    switch textField {
    case nameTextField:
      emailTextField.becomeFirstResponder()
    case emailTextField:
      passwordTextField.becomeFirstResponder()
    case passwordTextField:
      passwordConfirmationTextField.becomeFirstResponder()
    case passwordConfirmationTextField:
      phoneTextField.becomeFirstResponder()
    case phoneTextField:
      phoneTextField.resignFirstResponder()
      save()
    default:
      break
    }
    
    return true
  }
}
