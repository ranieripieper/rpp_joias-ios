//
//  MenuViewController.swift
//  Joias
//
//  Created by Gilson Gil on 2/21/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class MenuViewController: UIViewController {
  @IBOutlet weak var tableView: UITableView!
  
  var mainNavigationController: MainNavigationController?
  
  func preselectIndex(index: Int) {
    let indexPath = NSIndexPath(forRow: index, inSection: 0)
    tableView.selectRowAtIndexPath(indexPath, animated: false, scrollPosition: .None)
    let cell = tableView.cellForRowAtIndexPath(indexPath)
    cell?.setSelected(true, animated: false)
  }
  
  func goToHome() {
    mainNavigationController?.goToHome()
  }
  
  func goToCollections() {
    mainNavigationController?.goToCollections()
  }
  
  func goToLooks() {
    mainNavigationController?.goToLooks()
  }
  
  func goToTry() {
    mainNavigationController?.goToTry()
  }
  
  func goToVideos() {
    mainNavigationController?.goToVideos()
  }
  
  func goToFacebook() {
    mainNavigationController?.goToFacebook()
  }
  
  func goToInstagram() {
    mainNavigationController?.goToInstagram()
  }
  
  func goToMyProfile() {
    mainNavigationController?.goToMyProfile()
  }
  
  func goToAbout() {
    mainNavigationController?.goToAbout()
  }
  
  func logout() {
    mainNavigationController?.logout()
  }
  
  func login() {
    mainNavigationController?.login()
  }
}
