//
//  TryViewController.swift
//  Joias
//
//  Created by Gilson Gil on 2/21/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class TryViewController: UIViewController {
  @IBOutlet weak var label: UILabel!
  @IBOutlet weak var userImageView: UIImageView!
  @IBOutlet weak var jewelImageView: TouchableImageView!
  @IBOutlet weak var okButton: UIButton!
  
  var originalImage: UIImage!
  var image: UIImage?
  var trans: CGAffineTransform!
  var jewel: Jewel!

  override func viewDidLoad() {
    super.viewDidLoad()
    label.textColor = UIColor.tryAdjustColor()
    label.font = UIFont.lightFontWithSize(14)
    let okAttributedTitle = NSAttributedString(string: "Ok", attributes: [NSForegroundColorAttributeName: UIColor.collectionButtonsColor(), NSFontAttributeName: UIFont.lightFontWithSize(22)])
    okButton.setAttributedTitle(okAttributedTitle, forState: .Normal)
    userImageView.image = originalImage
    userImageView.addSubview(jewelImageView)
    
    jewelImageView.image = UIImage(named: jewel.picture.URL.stringByReplacingOccurrencesOfString("thumb_", withString: ""))
    
    let pinch = UIPinchGestureRecognizer(target: jewelImageView, action: "handlePinch:")
    view.addGestureRecognizer(pinch)
    
    let rotation = UIRotationGestureRecognizer(target: jewelImageView, action: "handleRotation:")
    view.addGestureRecognizer(rotation)
  }
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
    if let tryFilterViewController = segue.destinationViewController as? TryFilterViewController {
      tryFilterViewController.image = snapshotView()
      tryFilterViewController.jewel = jewel
      if let image = image {
        tryFilterViewController.image = image
      } else {
        tryFilterViewController.image = snapshotView()
      }
    }
  }
  
  func snapshotView() -> UIImage {
    UIGraphicsBeginImageContextWithOptions(userImageView.bounds.size, false, 0)
    let context = UIGraphicsGetCurrentContext()
    userImageView.layer.renderInContext(context)
    let image = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    return image
  }
  
  @IBAction func doneTapped(sender: UIButton?) {
    performSegueWithIdentifier("SegueFilter", sender: nil)
  }
}
