//
//  CollectionViewController.swift
//  Joias
//
//  Created by Gilson Gil on 3/14/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class CollectionViewController: UIViewController {
  @IBOutlet weak var collectionView: UICollectionView!
  @IBOutlet weak var spinner: Spinner!
  @IBOutlet weak var collectionDelegate: CollectionDelegate!
  @IBOutlet weak var spinnerBottomSpaceConstraint: NSLayoutConstraint!
  
  var collection = Jewel.Collection.None
  var currentPage = 0
  var loading: Bool = false {
    didSet {
      if loading {
        spinner.startAnimating()
      } else {
        spinner.stopAnimating()
      }
    }
  }
  var endReached = false
  
  override func viewDidLoad() {
    super.viewDidLoad()
    collectionView.collectionViewLayout = collectionDelegate.stackedGridLayout!
    configureView()
    getJewels()
    navigationItem.title = collection.toString()
  }
  
  func configureView() {
    collectionView.registerNib(UINib(nibName: "LoadingCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "LoadingCollectionViewCell")
    collectionView.contentInset = UIEdgeInsets(top: 64, left: 0, bottom: 80, right: 0)
    collectionView.scrollIndicatorInsets = UIEdgeInsets(top: 64, left: 0, bottom: 0, right: 0)
  }
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
    if let jewelDetailCollectionViewController = segue.destinationViewController as? JewelDetailCollectionViewController, index = sender as? Int {
      jewelDetailCollectionViewController.jewels = collectionDelegate.datasource
      jewelDetailCollectionViewController.currentIndex = index
    }
  }
  
  func getJewels() {
    if loading || endReached {
      return
    }
    loading = true
    switch collection {
    case .None:
      API.homeItems(self.currentPage + 1, result: callback)
    case .New:
      API.newItems(self.currentPage + 1, result: callback)
    case .Classic:
      API.classicItems(self.currentPage + 1, result: callback)
    }
  }
  
  func callback(result: Result<[Jewel]>) -> () {
    switch result {
    case .Success(let boxed):
      if self.currentPage == 0 {
        collectionDelegate.datasource = []
      }
      if boxed.unbox.count == 0 {
        self.endReached = true
        break
      }
      collectionDelegate.datasource += boxed.unbox
      collectionView.reloadData()
      self.currentPage++
    case .Failure(let error):
      UIView.animateWithDuration(0.3) {
        self.collectionView.contentInset = UIEdgeInsets(top: 64, left: 0, bottom: 0, right: 0)
      }
    }
    loading = false
  }
  
  func goToJewelDetailsAtIndex(index: Int) {
    performSegueWithIdentifier("SegueDetail", sender: index)
  }
}
