//
//  TryCameraViewController.swift
//  Joias
//
//  Created by Gilson Gil on 2/21/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class TryCameraViewController: UIViewController {
  @IBOutlet weak var directionsLabel: UILabel!
  @IBOutlet weak var overlayImageView: UIImageView!
  @IBOutlet weak var turnCameraButton: UIButton?
  @IBOutlet weak var captureView: CaptureView!
  
  var jewel: Jewel?
  var alertView: AlertController?

  override func viewDidLoad() {
    super.viewDidLoad()
    directionsLabel.font = UIFont.lightFontWithSize(12)
    configureWithCategory(jewel!.category)
    if !captureView.canChangeVideoInput() {
      turnCameraButton?.removeFromSuperview()
    }
  }
  
  override func viewDidAppear(animated: Bool) {
    super.viewDidAppear(animated)
    captureView.addVideoInput()
    captureView.addStillImageOutput()
    captureView.addVideoPreviewLayer()
    captureView.captureSession.startRunning()
  }
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
    if let tryViewController = segue.destinationViewController as? TryViewController {
      tryViewController.jewel = jewel
      if let image = sender as? UIImage {
        tryViewController.originalImage = image
      }
    }
  }
  
  func configureWithCategory(category: Jewel.Category) {
    switch category {
    case .Earrings:
      directionsLabel.text = "Encaixe seu rosto diagonalmente na forma abaixo e aperte o botão. Lembre-se de deixar sua orelha visível."
      overlayImageView.image = UIImage(named: "img_brinco")
    case .Necklace:
      directionsLabel.text = "Encaixe seu pescoço na imagem e aperte o botão abaixo."
      overlayImageView.image = UIImage(named: "img_colar")
    case .Ring:
      directionsLabel.text = "Encaixe sua mão na forma abaixo e aperte o botão."
      overlayImageView.image = UIImage(named: "img_anel")
    case .Bracelet:
      directionsLabel.text = "Encaixe seu braço na forma abaixo e aperte o botão."
      overlayImageView.image = UIImage(named: "img_pulseira")
    case .None:
      break
    }
  }
  
  @IBAction func takePicture(sender: UIButton) {
    captureView.captureStillImage { image in
      if let image = image {
        self.performSegueWithIdentifier("SegueView", sender: image)
      } else {
        self.alertView = AlertController(title: "", message: "Ocorreu um erro ao tirar a foto, por favor tente novamente.", buttons: nil, cancelButton: ("Ok", {
          self.alertView = nil
          self.captureView.captureSession.startRunning()
        }), style: .Alert)
        self.alertView!.alertInViewController(self)
      }
    }
  }
  
  @IBAction func turnCamera(sender: UIButton) {
    captureView.changeVideoInput()
  }
}
