//
//  AboutViewController.swift
//  Joias
//
//  Created by Gilson Gil on 2/21/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class AboutViewController: UIViewController {
  @IBOutlet weak var titleLabel: UILabel!
  @IBOutlet weak var textView: UITextView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    navigationItem.title = Constants.appName
    titleLabel.text = "Sobre a \(Constants.appName)"
    titleLabel.textColor = UIColor.navigationBarColor()
    textView.text = Constants.aboutText
  }
  
  @IBAction func toggleMenu(sender: UIBarButtonItem) {
    (navigationController as! MainNavigationController).toggleMenu()
  }
}
