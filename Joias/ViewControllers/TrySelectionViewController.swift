//
//  TrySelectionViewController.swift
//  Joias
//
//  Created by Gilson Gil on 3/12/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class TrySelectionViewController: UIViewController {
  @IBOutlet weak var collectionView: UICollectionView!
  @IBOutlet weak var trySelectionCollectionViewDelegate: TrySelectionCollectionViewDelegate!
  
  var jewelCategory: Jewel.Category? {
    didSet {
      switch jewelCategory! {
      case .Bracelet:
        trySelectionCollectionViewDelegate.datasource = Jewel.tryBracelets()
      case .Ring:
        trySelectionCollectionViewDelegate.datasource = Jewel.tryRings()
      case .Earrings:
        trySelectionCollectionViewDelegate.datasource = Jewel.tryEarrings()
      case .Necklace:
        trySelectionCollectionViewDelegate.datasource = Jewel.tryNecklaces()
      case .None:
        break
      }
    }
  }
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
    if let tryCameraViewController = segue.destinationViewController as? TryCameraViewController {
      if let jewel = sender as? Jewel {
        tryCameraViewController.jewel = jewel
      }
    }
  }
  
  func try(jewel: Jewel) {
    performSegueWithIdentifier("SegueTry", sender: jewel)
  }
}
