//
//  LoginNavigationController.swift
//  Joias
//
//  Created by Gilson Gil on 4/18/16.
//  Copyright (c) 2016 doisdoissete. All rights reserved.
//

import UIKit

final class LoginNavigationController: UINavigationController {
  override func preferredStatusBarStyle() -> UIStatusBarStyle {
    
    return .LightContent
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    configureView()
  }
  
  private func configureView() {
    navigationBar.barTintColor = UIColor.navigationBarBackgroundColor()
    navigationBar.tintColor = UIColor.navigationBarColor()
    navigationBar.titleTextAttributes = [NSFontAttributeName: UIFont.regularFontWithSize(22), NSForegroundColorAttributeName: UIColor.navigationBarColor()]
  }
}
