//
//  LoginAlertViewController.swift
//  Joias
//
//  Created by Gilson Gil on 4/18/16.
//  Copyright (c) 2016 doisdoissete. All rights reserved.
//

import UIKit

final class LoginAlertViewController: UIViewController {
  @IBOutlet private weak var alertView: UIView?
  @IBOutlet private weak var label: UILabel?
  @IBOutlet private weak var line: UIView?
  
  private var text: String?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    configureView()
  }
  
  private func configureView() {
    alertView?.backgroundColor = UIColor.loginAlertBackgroundColor()
    line?.backgroundColor = UIColor.loginAlertLineColor()
    
    label?.text = text
  }
  
  func configureWithText(text: String) {
    self.text = text
    label?.text = text
  }
}

extension LoginAlertViewController {
  @IBAction func okButtonTapped(sender: UIButton) {
    dismissViewControllerAnimated(false, completion: nil)
  }
}
