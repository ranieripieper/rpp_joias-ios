//
//  Constants.swift
//  Joias
//
//  Created by Gilson Gil on 3/11/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import Foundation

struct Constants {
  typealias Color = (red: Float, green: Float, blue: Float)
  
  static let appName = "Aulore"
  
  static let serverURL = "http://54.207.96.156:91/"
  static let serverToken = "6a6efa5f-98d0-4704-a50f-35e9b5bbdca1"

  static let instagramUserId = "184406051"
  static let facebookUserId = "auloreherreira"
  
  static let appStoreLink = "https://goo.gl/qWGV55"

  static let instagramClientId = "148078b7fa5745f6bd890aa18b1f5ae9"
  static let facebookAppId = "314656991967495"
  static let facebookAppSecret = "7e0a8f56be9ad870b06db6b0267f8470"
  
  static let parseApplicationId = "8rX0t9vUYrOTBRo7OG8evatuHraMz4bTcujF44iP"
  static let parseClientKey = "sehzgM3ZlFok1YC9qEglCukmPmvJvacA3SRs5i66"
  
  static let navigationBarBackgroundColor: Color = (red: 155 / 255, green: 81 / 255, blue: 106 / 255)
  static let navigationBarColor: Color = (red: 231 / 255, green: 182 / 255, blue: 180 / 255)
  static let collectionCategoryColor: Color = (red: 155 / 255, green: 81 / 255, blue: 106 / 255)
  static let loadingBottomColor: Color = (red: 231 / 255, green: 182 / 255, blue: 180 / 255)
  static let loadingTopColor: Color = (red: 155 / 255, green: 81 / 255, blue: 106 / 255)
  static let collectionButtonsColor: Color = (red: 231 / 255, green: 182 / 255, blue: 180 / 255)
  static let tryTextColor: Color = (red: 155 / 255, green: 81 / 255, blue: 106 / 255)
  static let tryAdjustColor: Color = (red: 155 / 255, green: 81 / 255, blue: 106 / 255)
  static let jewelCategoryColor: Color = (red: 155 / 255, green: 81 / 255, blue: 106 / 255)
  static let interestBackgroundColor: Color = (red: 231 / 255, green: 182 / 255, blue: 180 / 255)
  static let loginAlertLineColor: Color = (red: 89.0/255.0, green: 70.0/255.0, blue: 77.0/255.0)
  static let loginAlertBackgroundColor: Color = (red: 124.0/255.0, green: 99.0/255.0, blue: 108.0/255.0)
  static let loginInvalidInputColor: Color = (red: 167.0/255.0, green: 104.0/255.0, blue: 99.0/255.0)
  static let loginTextFieldLabelColor: Color = (red: 98.0/255.0, green: 101.0/255.0, blue: 103.0/255.0)
  
  static let regularFont = "GillSans"
  static let lightFont = "GillSans-Light"
  static let mediumFont = "GillSans-SemiBold"
  static let semiboldFont = "GillSans-SemiBold"
  static let tryFont = "GillSans-Light"
  
  static let aboutText = "Fábrica de semijoias de luxo, inaugurada a 4 anos, a partir do desejo de proporcionar as mulheres, um nível superior em design e sofisticação, ainda inexistente no mercado nacional. Responsável por todas as etapas do processo produtivo, desde a concepção dos modelos até o acabamento, concentra seus esforços em antecipar e converter as principais tendências da moda em suas joias.\n\nEm seu DNA institucional, a Aulore tem o respeito e a valorização de suas relações profissionais e comerciais com colaboradores, parceiros, revendedores e clientes finais, como principal valor e busca, constantemente, evoluir nesse sentido.\n\nConheça mais e torne-se um parceiro, entrando em contato através dos telefones, (11) 2615 0978 ou (62) 3558 5858, ou ainda, através do e-mail, contato@aulore.com.br."
  
  static let earringsCount = 35
  static let ringsCount = 30
}
