//
//  Error.swift
//
//  Created by Gilson Gil on 2/13/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import Foundation

struct Error {
  typealias ErrorInfoDictionary = Dictionary<String, Any>
  
  /// The error code used to differentiate between various error states.
  let code: Int
  
  /// A string that is used to group errors into related error buckets.
  let domain: String
  
  /// A place to store any custom information that needs to be passed along with the error instance.
  let userInfo: ErrorInfoDictionary
  
  /// Initializes a new `Error` instance.
  init(code: Int, domain: String, userInfo: ErrorInfoDictionary?) {
    self.code = code
    self.domain = domain
    if let info = userInfo {
      self.userInfo = info
    }
    else {
      self.userInfo = [String:Any]()
    }
  }
}
