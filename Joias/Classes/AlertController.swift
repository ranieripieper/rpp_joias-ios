//
//  AlertController.swift
//  GuiaCafe
//
//  Created by Gilson Gil on 12/16/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

typealias StringVoidType = (String, (() -> ())?)

class AlertController: NSObject {
  var alertController: UIAlertController?
  var alertView: UIAlertView?
  var actionSheet: UIActionSheet?
  var title: String?
  var message: String?
  var buttons: [StringVoidType]?
  var cancelButton: StringVoidType?
  var style: UIAlertControllerStyle
  
  init(title: String?, message: String?, buttons: [StringVoidType]?, cancelButton: StringVoidType?, style: UIAlertControllerStyle) {
    self.title = title
    self.message = message
    self.buttons = buttons
    self.cancelButton = cancelButton
    self.style = style
    if IOS_8() {
      alertController = UIAlertController(title: title, message: message, preferredStyle: style)
    } else {
      switch style {
      case .Alert:
        alertView = UIAlertView(title: title, message: message, delegate: nil, cancelButtonTitle: cancelButton?.0)
      case .ActionSheet:
        actionSheet = UIActionSheet(title: title, delegate: nil, cancelButtonTitle: cancelButton?.0, destructiveButtonTitle: nil)
      }
    }
  }
  
  func addTextFieldWithConfigurationHandler(handler: ((textField: UITextField!) -> Void)?) {
    if handler != nil {
      if IOS_8() {
        alertController!.addTextFieldWithConfigurationHandler(handler)
      } else if style == .Alert {
        if alertView!.alertViewStyle == UIAlertViewStyle.Default {
          alertView!.alertViewStyle = .PlainTextInput
        } else if alertView!.alertViewStyle == .PlainTextInput {
          alertView!.alertViewStyle = .LoginAndPasswordInput
        }
      }
    }
  }
  
  func alertInViewController(viewController: UIViewController) {
    if IOS_8() {
      if cancelButton != nil {
        alertController!.addAction(UIAlertAction(title: cancelButton!.0, style: .Cancel, handler: { (alertAction) -> Void in
          if self.cancelButton!.1 != nil {
            self.cancelButton!.1!()
          }
        }))
      }
      if buttons != nil {
        for tuple in buttons! {
          let buttonTitle = tuple.0
          let closure = tuple.1
          alertController!.addAction(UIAlertAction(title: buttonTitle, style: .Default, handler: { (alertAction) -> Void in
            if closure != nil {
              closure!()
            }
          }))
        }
      }
      viewController.presentViewController(alertController!, animated: true, completion: nil)
    } else if style == .Alert {
      if buttons != nil {
        for tuple in buttons! {
          alertView!.addButtonWithTitle(tuple.0)
        }
      }
      alertView!.delegate = self
      alertView!.show()
    } else if style == .ActionSheet {
      if buttons != nil {
        for tuple in buttons! {
          actionSheet!.addButtonWithTitle(tuple.0)
        }
      }
      actionSheet!.delegate = self
      actionSheet!.showInView(viewController.view)
    }
  }
  
  func textFieldTextAtIndex(index: Int) -> String? {
    if IOS_8() {
      if alertController!.textFields != nil && alertController!.textFields!.count > index {
        return (alertController!.textFields![index] as! UITextField).text
      }
    } else {
      if alertView!.textFieldAtIndex(index) != nil {
        return alertView!.textFieldAtIndex(index)!.text
      }
    }
    return nil
  }
}

extension AlertController: UIAlertViewDelegate {
  func alertView(alertView: UIAlertView, didDismissWithButtonIndex buttonIndex: Int) {
    if buttonIndex == alertView.cancelButtonIndex {
      cancelButton?.1?()
    } else {
      buttons![buttonIndex - (cancelButton != nil ? 1 : 0)].1?()
    }
  }
}

extension AlertController: UIActionSheetDelegate {
  func actionSheet(actionSheet: UIActionSheet, willDismissWithButtonIndex buttonIndex: Int) {
    actionSheet.delegate = nil
    if buttonIndex == actionSheet.cancelButtonIndex {
      cancelButton?.1?()
    } else {
      buttons![buttonIndex - (cancelButton != nil ? 1 : 0)].1?()
    }
  }
}

func IOS_8() -> Bool {
  switch UIDevice.currentDevice().systemVersion.compare("8.0.0", options: NSStringCompareOptions.NumericSearch) {
  case .OrderedSame, .OrderedDescending:
    return true
  case .OrderedAscending:
    return false
  }
}
