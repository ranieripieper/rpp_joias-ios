//
//  Colors.swift
//  Joias
//
//  Created by Gilson Gil on 3/12/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

extension UIColor {
  class func navigationBarBackgroundColor() -> UIColor {
    return UIColor.colorWithColor(Constants.navigationBarBackgroundColor)
  }
  
  class func navigationBarColor() -> UIColor {
    return UIColor.colorWithColor(Constants.navigationBarColor)
  }
  
  class func collectionCategoryColor() -> UIColor {
    return UIColor.colorWithColor(Constants.collectionCategoryColor)
  }
  
  class func loadingBottomColor() -> UIColor {
    return UIColor.colorWithColor(Constants.loadingBottomColor)
  }
  
  class func loadingTopColor() -> UIColor {
    return UIColor.colorWithColor(Constants.loadingTopColor)
  }
  
  class func collectionButtonsColor() -> UIColor {
    return UIColor.colorWithColor(Constants.collectionButtonsColor)
  }
  
  class func tryTextColor() -> UIColor {
    return UIColor.colorWithColor(Constants.tryTextColor)
  }
  
  class func tryAdjustColor() -> UIColor {
    return UIColor.colorWithColor(Constants.tryAdjustColor)
  }
  
  class func jewelCategoryColor() -> UIColor {
    return UIColor.colorWithColor(Constants.jewelCategoryColor)
  }
  
  class func loginAlertLineColor() -> UIColor {
    return UIColor.colorWithColor(Constants.loginAlertLineColor)
  }
  
  class func loginAlertBackgroundColor() -> UIColor {
    return UIColor.colorWithColor(Constants.loginAlertBackgroundColor)
  }
  
  class func loginInvalidInputColor() -> UIColor {
    return UIColor.colorWithColor(Constants.loginInvalidInputColor)
  }
  
  class func loginTextFieldLabelColor() -> UIColor {
    return UIColor.colorWithColor(Constants.loginTextFieldLabelColor)
  }
  
  class func interestBackgroundColor() -> UIColor {
    let color = Constants.interestBackgroundColor
    return UIColor(red: CGFloat(color.red), green: CGFloat(color.green), blue: CGFloat(color.blue), alpha: 0.95)
  }
  
  class func colorWithColor(color: Constants.Color) -> UIColor {
    return UIColor(red: CGFloat(color.red), green: CGFloat(color.green), blue: CGFloat(color.blue), alpha: 1)
  }
}
