//
//  HomeAPI.swift
//  Joias
//
//  Created by Gilson Gil on 5/11/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import Foundation

struct API {
  static func homeItems(page: Int, result: (Result<[Jewel]> -> ())) {
    request(Networking.Router.Home(page))
      .responseCollection { (_, _, jewels: Box<[Jewel]>?, error: NSError?) in
        if error == nil {
          result(Result(jewels?.unbox ?? []))
        } else {
          result(Result(Error(code: 400, domain: "com.doisdoissete.Joias", userInfo: ["description": error?.description])))
        }
    }
  }
  
  static func newItems(page: Int, result: (Result<[Jewel]> -> ())) {
    request(Networking.Router.Collections("lancamentos", page))
      .responseCollection { (_, _, jewels: Box<[Jewel]>?, error: NSError?) in
        if error == nil {
          result(Result(jewels?.unbox ?? []))
        } else {
          result(Result(Error(code: 400, domain: "com.doisdoissete.Joias", userInfo: ["description": error?.description])))
        }
    }
  }
  
  static func classicItems(page: Int, result: (Result<[Jewel]> -> ())) {
    request(Networking.Router.Collections("classicas", page))
      .responseCollection { (_, _, jewels: Box<[Jewel]>?, error: NSError?) in
        if error == nil {
          result(Result(jewels?.unbox ?? []))
        } else {
          result(Result(Error(code: 400, domain: "com.doisdoissete.Joias", userInfo: ["description": error?.description])))
        }
    }
  }
  
  static func looksPressItems(page: Int, result: (Result<[Jewel]> -> ())) {
    request(Networking.Router.LooksPress(page))
      .responseCollection { (_, _, jewels: Box<[Jewel]>?, error: NSError?) in
        if error == nil {
          result(Result(jewels?.unbox ?? []))
        } else {
          result(Result(Error(code: 400, domain: "com.doisdoissete.Joias", userInfo: ["description": error?.description])))
        }
    }
  }
  
  static func videoItems(page: Int, result: (Result<[Video]> -> ())) {
    request(Networking.Router.Videos(page))
      .responseCollection { (_, _, videos: Box<[Video]>?, error: NSError?) in
        if error == nil {
          result(Result(videos?.unbox ?? []))
        } else {
          result(Result(Error(code: 400, domain: "com.doisdoissete.Joias", userInfo: ["description": error?.description])))
        }
    }
  }
  
  static func sendEmail(name: String, email: String, phone: String, city: String?, state: String?, jewelName: String, jewelURL: String, result: Result<Bool> -> ()) {
    request(Networking.Router.Email(name, email, phone, city, state, jewelName, jewelURL))
      .response() { req, res, data, error in
        if error == nil {
          result(Result(true))
        } else {
          result(Result(Error(code: error!.code, domain: error!.domain, userInfo: ["description": error!.localizedDescription])))
        }
    }
  }
}