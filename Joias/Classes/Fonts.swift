//
//  Fonts.swift
//  Joias
//
//  Created by Gilson Gil on 3/12/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

extension UIFont {
  class func regularFontWithSize(size: CGFloat) -> UIFont {
    return UIFont(name: Constants.regularFont, size: size)!
  }
  
  class func lightFontWithSize(size: CGFloat) -> UIFont {
    return UIFont(name: Constants.lightFont, size: size)!
  }
  
  class func mediumFontWithSize(size: CGFloat) -> UIFont {
    return UIFont(name: Constants.mediumFont, size: size)!
  }
  
  class func semiboldFontWithSize(size: CGFloat) -> UIFont {
    return UIFont(name: Constants.semiboldFont, size: size)!
  }
  
  class func tryFont() -> UIFont {
    return UIFont(name: Constants.tryFont, size: 16)!
  }
}
