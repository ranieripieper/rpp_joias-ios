//
//  FacebookAPI.swift
//  Joias
//
//  Created by Gilson Gil on 2/22/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import Foundation

struct FacebookAPI {
  static func mediasForURLString(urlString: String?, result: (Result<([Post],String?)> -> ())) {
    let accessToken = Defaults["FacebookAccessToken"].string
    if accessToken != nil {
      request(Networking.Router.Facebook(urlString, accessToken!))
        .responseCollectionPosts { (_, _, posts: Box<[Post]>?, nextPage, error: NSError?) in
          if error == nil {
            let tuple = (posts?.unbox ?? [], nextPage as? String)
            result(Result(tuple))
          } else {
            result(Result(Error(code: 400, domain: "com.doisdoissete.Joias", userInfo: ["description": error?.description])))
          }
      }
    } else {
      result(Result(Error(code: 403, domain: "com.doisdoissete.Joias", userInfo: ["description": "invalid access token"])))
    }
  }
  
  static func largeImage(postId: String, result: Result<String> -> ()) {
    let accessToken = Defaults["FacebookAccessToken"].string
    if accessToken != nil {
      request(Networking.Router.FacebookPost(postId, accessToken!))
        .responseJSON(completionHandler: { (_, _, data, error) -> Void in
          if let dataResponse = data as? [String: AnyObject] {
            if let images = dataResponse["images"] as? [[String: AnyObject]] {
              if let imageUrl = images.first?["source"] as? String {
                result(Result(imageUrl))
                return
              }
            }
          } else {
            result(Result(Error(code: 400, domain: "com.doisdoissete.Joias", userInfo: ["description": error?.description])))
          }
        })
    } else {
      result(Result(Error(code: 403, domain: "com.doisdoissete.Joias", userInfo: ["description": "invalid access token"])))
    }
  }
  
  static func likes(postId: String, result: Result<Int> -> ()) {
    let accessToken = Defaults["FacebookAccessToken"].string
    if accessToken != nil {
      request(Networking.Router.FacebookLikes(postId, accessToken!))
        .responseJSON(completionHandler: { _, _, data, error in
          if let dataDict = data as? [String: AnyObject] {
            if let summary = dataDict["summary"] as? [String: AnyObject] {
              if let count = summary["total_count"] as? Int {
                result(Result(count))
                return
              }
            }
          }
          result(Result(Error(code: 400, domain: "com.doisdoissete.Joias", userInfo: ["description": error?.description])))
        })
    } else {
      result(Result(Error(code: 403, domain: "com.doisdoissete.Joias", userInfo: ["description": "invalid access token"])))
    }
  }
  
  static func comments(postId: String, result: Result<Int> -> ()) {
    let accessToken = Defaults["FacebookAccessToken"].string
    if accessToken != nil {
      request(Networking.Router.FacebookComments(postId, accessToken!))
        .responseJSON(completionHandler: { _, _, data, error in
          if let dataDict = data as? [String: AnyObject] {
            if let summary = dataDict["summary"] as? [String: AnyObject] {
              if let count = summary["total_count"] as? Int {
                result(Result(count))
                return
              }
            }
          }
          result(Result(Error(code: 400, domain: "com.doisdoissete.Joias", userInfo: ["description": error?.description])))
        })
    } else {
      result(Result(Error(code: 403, domain: "com.doisdoissete.Joias", userInfo: ["description": "invalid access token"])))
    }
  }
  
  static func accessToken(result: Result<String> -> ()) {
    if let accessToken = Defaults["FacebookAccessToken"].string {
      result(Result(accessToken))
    } else {
      request(Networking.Router.FacebookAccessToken)
        .response({ _, _, data, error in
          if let dataString = data as? NSData {
            if let string: String = NSString(data: dataString, encoding: NSUTF8StringEncoding) as? String {
              let accessToken = string.stringByReplacingOccurrencesOfString("access_token=", withString: "")
              Defaults["FacebookAccessToken"] = accessToken
              Defaults.synchronize()
              result(Result(accessToken))
              return
            }
          }
          result(Result(Error(code: 400, domain: "com.doisdoissete.Joias", userInfo: ["description": error?.description])))
        })
    }
  }
}