//
//  Array+Extension.swift
//  Joias
//
//  Created by Gilson Gil on 2/22/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import Foundation

extension Array {
  mutating func addElementsOf(newArray: Array) {
    for element in newArray {
      append(element)
    }
  }
}