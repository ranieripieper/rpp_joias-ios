//
//  Networking.swift
//
//  Created by Gilson Gil on 2/13/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

@objc protocol ResponseObjectSerializable {
  init(response: NSHTTPURLResponse, representation: AnyObject)
}

protocol ResponseCollectionSerializable {
  static func collection(#response: NSHTTPURLResponse, representation: AnyObject) -> [Self]
}

extension Request {
  class func imageResponseSerializer() -> Serializer {
    return { request, response, data in
      if data == nil {
        return (nil, nil)
      }
      
      let image = UIImage(data: data!, scale: UIScreen.mainScreen().scale)
      
      return (image, nil)
    }
  }
  
  func responseImage(completionHandler: (NSURLRequest, NSHTTPURLResponse?, UIImage?, NSError?) -> Void) -> Self {
    return response(serializer: Request.imageResponseSerializer(), completionHandler: { (request, response, image, error) in
      completionHandler(request, response, image as? UIImage, error)
    })
  }
}

extension Request {
  func responseObject<T: ResponseObjectSerializable>(completionHandler: (NSURLRequest, NSHTTPURLResponse?, T?, NSError?) -> Void) -> Self {
    let serializer: Serializer = { (request, response, data) in
      let JSONSerializer = Request.JSONResponseSerializer(options: .AllowFragments)
      let (JSON: AnyObject?, serializationError) = JSONSerializer(request, response, data)
      if response != nil && JSON != nil {
        return (T(response: response!, representation: JSON!), nil)
      } else {
        return (nil, serializationError)
      }
    }
    
    return response(serializer: serializer, completionHandler: { (request, response, object, error) in
      completionHandler(request, response, object as? T, error)
    })
  }
}

extension Request {
  func responseCollectionPosts<T: ResponseCollectionSerializable>(completionHandler: (NSURLRequest, NSHTTPURLResponse?, Box<[T]>?, AnyObject?, NSError?) -> Void) -> Self {
    var nextPage: String?
    let serializer: Serializer = { (request, response, data) in
      let JSONSerializer = Request.JSONResponseSerializer(options: .AllowFragments)
      let (JSON: AnyObject?, serializationError) = JSONSerializer(request, response, data)
      nextPage = JSON?.valueForKeyPath("pagination.next_url") as? String ?? JSON?.valueForKeyPath("paging.next") as? String
      if response != nil && JSON != nil {
        return (Box(T.collection(response: response!, representation: JSON!)), nil)
      } else {
        return (nil, serializationError)
      }
    }
    
    return response(serializer: serializer) { (request, response, object, error) in
      completionHandler(request, response, object as? Box<[T]>, nextPage, error)
    }
  }
  
  func responseCollection<T: ResponseCollectionSerializable>(completionHandler: (NSURLRequest, NSHTTPURLResponse?, Box<[T]>?, NSError?) -> Void) -> Self {
    let serializer: Serializer = { request, response, data in
      let JSONSerializer = Request.JSONResponseSerializer(options: .AllowFragments)
      let (JSON: AnyObject?, serializationError) = JSONSerializer(request, response, data)
      if response != nil && JSON != nil {
        return (Box(T.collection(response: response!, representation: JSON!)), nil)
      } else {
        return (nil, serializationError)
      }
    }
    
    return response(serializer: serializer) { request, response, object, error in
      completionHandler(request, response, object as? Box<[T]>, error)
    }
  }
}

struct Networking {
  enum Router: URLRequestConvertible {
    static let instagramBaseURLString = "https://api.instagram.com/v1/"
    static let facebookBaseURLString = "https://graph.facebook.com/v2.2/"
    
    static let perPage = 50
    
    case Home(Int)
    case Collections(String, Int)
    case LooksPress(Int)
    case Videos(Int)
    case Email(String, String, String, String?, String?, String, String)
    
    case Instagram(String?)
    case Facebook(String?, String)
    case FacebookPost(String, String)
    case FacebookLikes(String, String)
    case FacebookComments(String, String)
    case FacebookAccessToken
    
    var method: Method {
      switch self {
      case .Email:
        return .POST
      default:
        return .GET
      }
    }
    
    var URLRequest: NSURLRequest {
      let (path: String, parameters: [String: AnyObject]?) = {
        switch self {
        case .Home(let page):
          return ("\(Constants.serverURL)home.json/\(page)/\(Networking.Router.perPage)/\(Constants.serverToken)/", nil)
        case .Collections(let collection, let page):
          return ("\(Constants.serverURL)colecao.json/\(page)/\(Networking.Router.perPage)/\(Constants.serverToken)/\(collection)", nil)
        case .LooksPress(let page):
          return ("\(Constants.serverURL)lookandpress.json/\(page)/\(Networking.Router.perPage)/\(Constants.serverToken)/", nil)
        case .Videos(let page):
          return ("\(Constants.serverURL)video.json/\(page)/\(Networking.Router.perPage)/\(Constants.serverToken)/", nil)
        case .Email(let name, let email, let phone, let city, let state, let jewelName, let jewelURL):
          var params = ["nome": name, "email": email, "telefone": phone, "nomejoia": jewelName, "urljoia": jewelURL]
          if let city = city {
            params["cidade"] = city
          }
          if let state = state {
            params["estado"] = state
          }
          return ("\(Constants.serverURL)sendemail.json/\(Constants.serverToken)/", params)
        case .Instagram(let urlString):
          if urlString != nil {
            return (urlString!, nil)
          } else {
            let params = ["client_id": Constants.instagramClientId]
            return ("\(Networking.Router.instagramBaseURLString)users/\(Constants.instagramUserId)/media/recent/", params)
          }
        case .Facebook(let urlString, let accessToken):
          if urlString != nil {
            return (urlString!, nil)
          } else {
            return ("\(Networking.Router.facebookBaseURLString)\(Constants.facebookUserId)/feed?access_token=\(accessToken)", nil)
          }
        case .FacebookPost(let postId, let accessToken):
          return ("\(Networking.Router.facebookBaseURLString)\(postId)?access_token=\(accessToken)", nil)
        case .FacebookLikes(let postId, let accessToken):
          return ("\(Networking.Router.facebookBaseURLString)\(postId)/likes?summary=1&access_token=\(accessToken)", nil)
        case .FacebookComments(let postId, let accessToken):
          return ("\(Networking.Router.facebookBaseURLString)\(postId)/comments?summary=1&access_token=\(accessToken)", nil)
        case .FacebookAccessToken:
          return ("https://graph.facebook.com/oauth/access_token?type=client_cred&client_id=\(Constants.facebookAppId)&client_secret=\(Constants.facebookAppSecret)", nil)
        }
        }()
      
      let URL = NSURL(string: path.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)!)
      var URLRequest = NSMutableURLRequest(URL: URL!)
      URLRequest.HTTPMethod = method.rawValue
      let encoding = ParameterEncoding.URL
      
      return encoding.encode(URLRequest, parameters: parameters).0
    }
  }
}