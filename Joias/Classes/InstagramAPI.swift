//
//  InstagramAPI.swift
//  Joias
//
//  Created by Gilson Gil on 2/21/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import Foundation

struct InstagramAPI {
  static func mediasForURLString(urlString: String?, result: (Result<([Post],String?)> -> ())) {
    request(Networking.Router.Instagram(urlString))
      .responseCollectionPosts { (_, _, posts: Box<[Post]>?, nextPage, error: NSError?) in
        if error == nil {
          let tuple = (posts?.unbox ?? [], nextPage as? String)
          result(Result(tuple))
        } else {
          result(Result(Error(code: 400, domain: "com.doisdoissete.Joias", userInfo: ["description": error?.description])))
        }
    }
  }
}