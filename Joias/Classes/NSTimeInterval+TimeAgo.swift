//
//  NSTimeInterval+TimeAgo.swift
//  Joias
//
//  Created by Gilson Gil on 2/22/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import Foundation

extension NSTimeInterval {
  func differenceFrom(timeInterval: NSTimeInterval) -> String {
    let difference = abs(self - timeInterval)
    if difference < 60 {
      return "just now"
    } else if difference < 60 * 2 {
      return "1 minute ago"
    } else if difference < 60 * 60 {
      return "\(Int(difference / 60)) minutes ago"
    } else if difference < 60 * 60 * 2 {
      return "1 hour ago"
    } else if difference < 60 * 60 * 24 {
      return "\(Int(difference / 60 / 60)) hours ago"
    } else if difference < 60 * 60 * 24 * 2 {
      return "1 day ago"
    } else if difference < 60 * 60 * 24 * 7 {
      return "\(Int(difference / 60 / 60 / 24)) days ago"
    } else if difference < 60 * 60 * 24 * 7 * 2 {
      return "1 week ago"
    } else if difference < 60 * 60 * 24 * 30 * 2 {
      return "1 month ago"
    } else if difference < 60 * 60 * 24 * 365 {
      return "\(Int(difference / 60 / 60 / 24 / 30)) months ago"
    } else if difference < 60 * 60 * 24 * 365 * 2 {
      return "1 year ago"
    } else {
      return "\(Int(difference < 60 / 60 / 24 / 365)) year ago"
    }
  }
}