//
//  ParseHelper.swift
//  Joias
//
//  Created by Gilson Gil on 9/8/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import Foundation

struct ParseHelper {
  static func setApplication() {
    Parse.setApplicationId(Constants.parseApplicationId, clientKey: Constants.parseClientKey)
  }
  
  static func registerForRemoteNotifications() {
    let application = UIApplication.sharedApplication()
    let userNotificationTypes: UIUserNotificationType = .Alert | .Sound
    let settings = UIUserNotificationSettings(forTypes: userNotificationTypes, categories: nil)
    application.registerUserNotificationSettings(settings)
    application.registerForRemoteNotifications()
  }
  
  static func didRegisterForRemoteNotificationsWithDeviceToken(deviceToken: NSData) {
    let installation = PFInstallation.currentInstallation()
    installation.setDeviceTokenFromData(deviceToken)
    installation.saveInBackground()
  }
  
  static func loggedInUser() -> PFUser? {
    return PFUser.currentUser()
  }
  
  static func save(name: String?, email: String, password: String, phone: String?, completion: (Bool, String?) -> ()) {
    let aUser: PFUser
    if let user = PFUser.currentUser() {
      aUser = user
    } else {
      aUser = PFUser()
    }
    aUser.username = email
    aUser.email = email
    if count(password) > 0 {
      aUser.password = password
    }
    aUser["name"] = name
    aUser["phone"] = phone
    let callback: (Bool, NSError?) -> () = { success, error in
      if success {
        aUser.pinInBackground()
        completion(true, nil)
      } else if let error = error {
        completion(false, error.localizedDescription)
      }
    }
    if let user = PFUser.currentUser() {
      aUser.saveInBackgroundWithBlock(callback)
    } else {
      aUser.signUpInBackgroundWithBlock(callback)
    }
  }
  
  static func forgotPassword(email: String, completion: (Bool, String?) -> ()) {
    PFUser.requestPasswordResetForEmailInBackground(email) { success, error in
      if success {
        completion(true, nil)
      } else if let error = error {
        if error.code == 205 {
          completion(false, "Nenhum usuário encontrado com este email.")
        } else {
          completion(false, error.localizedDescription)
        }
      }
    }
  }
  
  static func login(email: String, password: String, completion: (Bool, String?) -> ()) {
    PFUser.logInWithUsernameInBackground(email, password: password) { user, error in
      if let user = user {
        user.pinInBackground()
        completion(true, nil)
      } else if let error = error {
        if error.code == 101 {
          completion(false, "Nenhum usuário encontrado com este email.")
        } else {
          completion(false, error.localizedDescription)
        }
      }
    }
  }
  
  static func logout() {
    PFUser.logOutInBackground()
  }
}
