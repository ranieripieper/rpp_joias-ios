//
//  VimeoHelper.swift
//  Joias
//
//  Created by Gilson Gil on 3/17/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class VimeoHelper: NSObject {
  var webView: UIWebView?
  var callback: (String -> ())?
  
  func getVimeoUrlWithUrlString(urlString: String, callback: (String -> ())?) {
    NSHTTPCookieStorage.sharedHTTPCookieStorage().cookieAcceptPolicy = .Always
    if let url = NSURL(string: urlString) {
      webView = UIWebView()
      webView!.loadRequest(NSURLRequest(URL: url))
      webView!.delegate = self
      self.callback = callback
    }
  }
  
  private func urlStringFromString(string: String) -> String? {
    var mutableString: String = string
    if let preRange = mutableString.rangeOfString("http") {
      mutableString.replaceRange(Range<String.Index>(start: mutableString.startIndex, end: preRange.startIndex), with: "")
      if let suRange = mutableString.rangeOfString("\"") {
        mutableString.replaceRange(Range<String.Index>(start: suRange.startIndex, end: mutableString.endIndex), with: "")
        return mutableString
      }
    }
    return nil
  }
}

extension VimeoHelper: UIWebViewDelegate {
  func webViewDidFinishLoad(webView: UIWebView) {
    let jsCode = "function getUrl() {var divs = document.getElementsByClassName('flideo cloaked');var result = '';for(var i=0; i < divs.length; i++) {result +=(divs[i].innerHTML);}return result;}getUrl()"
    if let urlString = webView.stringByEvaluatingJavaScriptFromString(jsCode) {
      if count(urlString) > 0 && callback != nil {
        callback?(urlStringFromString(urlString) ?? "error")
      }
    }
  }
  
  func webView(webView: UIWebView, didFailLoadWithError error: NSError) {
    callback?("error")
  }
}
