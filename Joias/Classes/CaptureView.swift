//
//  CaptureView.swift
//  Joias
//
//  Created by Gilson Gil on 3/12/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit
import AVFoundation
import ImageIO

class CaptureView: UIView {
  let captureSession: AVCaptureSession
  var stillImageOutput: AVCaptureStillImageOutput?
  weak var videoPreviewLayer: AVCaptureVideoPreviewLayer?
  var stillImage: UIImage?
  
  required init(coder aDecoder: NSCoder) {
    captureSession = AVCaptureSession()
    captureSession.sessionPreset = AVCaptureSessionPresetPhoto
    super.init(coder: aDecoder)
  }
  
  // MARK: Capture Session Configuration
  func addVideoPreviewLayer() {
    videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
    videoPreviewLayer!.videoGravity = AVLayerVideoGravityResizeAspectFill
    videoPreviewLayer!.frame = bounds
    layer.addSublayer(videoPreviewLayer!)
  }
  
  func removeVideoPreviewLayer() {
    videoPreviewLayer?.removeFromSuperlayer()
    videoPreviewLayer = nil
  }
  
  func addVideoInput() {
    if let videoDevice = AVCaptureDevice.defaultDeviceWithMediaType(AVMediaTypeVideo) {
      var error: NSError?
      if let videoIn = AVCaptureDeviceInput.deviceInputWithDevice(videoDevice, error: &error) as? AVCaptureDeviceInput {
        if error == nil {
          if captureSession.canAddInput(videoIn) {
            captureSession.addInput(videoIn)
          } else {
            println("Couldn't add video input")
          }
        } else {
          println("Couldn't create video input")
        }
      } else {
        println("Couldn't create video capture device")
      }
    }
  }
  
  func removeVideoInput() {
    captureSession.removeInput(captureSession.inputs.first as! AVCaptureDeviceInput)
  }
  
  func addStillImageOutput() {
    stillImageOutput = AVCaptureStillImageOutput()
    stillImageOutput?.outputSettings = [AVVideoCodecKey: AVVideoCodecJPEG]
    captureSession.addOutput(stillImageOutput)
  }
  
  func removeStillImageOutput() {
    captureSession.removeOutput(stillImageOutput)
    stillImageOutput = nil
  }
  
  // MARK: Methods
  func captureStillImage(completion: (UIImage?) -> ()) {
    var videoConnection: AVCaptureConnection?
    for connection in stillImageOutput!.connections {
      for port in connection.inputPorts! {
        if port.mediaType == AVMediaTypeVideo {
          videoConnection = connection as? AVCaptureConnection
          break
        }
      }
      if videoConnection != nil {
        break
      }
    }
    if !captureSession.running || videoConnection == nil || !videoConnection!.active {
      completion(nil)
      return
    }
    stillImageOutput?.captureStillImageAsynchronouslyFromConnection(videoConnection) { imageDataSampleBuffer, error in
      if CMGetAttachment(imageDataSampleBuffer, kCGImagePropertyExifDictionary, nil) == nil {
        return
      }
      let imageData = AVCaptureStillImageOutput.jpegStillImageNSDataRepresentation(imageDataSampleBuffer)
      var image = UIImage(data: imageData)
      var cropFrame = CGRect(x: (image!.size.height - image!.size.width) / 2, y: 0, width: image!.size.width, height: image!.size.width)
      let imageRef = CGImageCreateWithImageInRect(image!.CGImage, cropFrame)
      let finalImage = UIImage(CGImage: imageRef, scale: image!.scale, orientation: self.getMirroredOrientation(image!.imageOrientation))
      self.stillImage = finalImage
      self.captureSession.stopRunning()
      self.removeVideoInput()
      self.removeStillImageOutput()
      self.removeVideoPreviewLayer()
      completion(finalImage)
    }
  }

  func getMirroredOrientation(orientation: UIImageOrientation) -> UIImageOrientation {
    if let input = captureSession.inputs.last as? AVCaptureDeviceInput {
      if input.device.position == .Back {
        return orientation
      }
    }
    switch orientation {
    case .Up:
      return .UpMirrored
    case .Left:
      return .RightMirrored
    case .Down:
      return .DownMirrored
    case .Right:
      return .LeftMirrored
    default:
      return orientation
    }
  }
  
  func canChangeVideoInput() -> Bool {
    let videoDevices = AVCaptureDevice.devicesWithMediaType(AVMediaTypeVideo)
    return videoDevices.count > 1
  }
  
  func changeVideoInput() {
    if !canChangeVideoInput() { return }
    
    let videoDevices = AVCaptureDevice.devicesWithMediaType(AVMediaTypeVideo).filter { device in
      device.position != self.captureSession.inputs.last!.device!.position
    }
    captureSession.removeInput(captureSession.inputs.last as! AVCaptureDeviceInput)
    
    if let videoDevice = videoDevices.last as? AVCaptureDevice {
      var error: NSError?
      let videoIn = AVCaptureDeviceInput.deviceInputWithDevice(videoDevice, error: &error) as! AVCaptureDeviceInput
      if error == nil {
        if captureSession.canAddInput(videoIn) {
          captureSession.addInput(videoIn)
        } else {
          println("Couldn't add video input")
        }
      } else {
        println("Couldn't create video input")
      }
    } else {
      println("Couldn't create video capture device")
    }
  }
}
