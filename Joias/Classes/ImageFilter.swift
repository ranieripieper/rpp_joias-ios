//
//  ImageFilter.swift
//  Joias
//
//  Created by Gilson Gil on 10/8/15.
//  Copyright © 2015 doisdoissete. All rights reserved.
//

import Foundation

enum ImageFilter {
  case Original, ColorCrossPolynomial, /*ColorCube, *//*ColorCubeColorSpace, */ColorInvert, /*ColorMap, */ColorMonochrome, /*ColorPosterize, FalseColor, MaskToAlpha, */MaximumComponent, MinimumComponent, PhotoEffectChrome, PhotoEffectFade, PhotoEffectInstant, PhotoEffectMono, PhotoEffectNoir, PhotoEffectProcess, PhotoEffectTonal, PhotoEffectTransfer, SepiaTone, Vignette, VignetteEffect
  
  static func allFilters() -> [ImageFilter] {
    
    return [.Original, .ColorCrossPolynomial, .ColorInvert, .ColorMonochrome, .MaximumComponent, .MinimumComponent, .PhotoEffectChrome, .PhotoEffectFade, .PhotoEffectInstant, .PhotoEffectMono, .PhotoEffectNoir, .PhotoEffectProcess, .PhotoEffectTonal, .PhotoEffectTransfer, .SepiaTone, .Vignette, .VignetteEffect]
  }
  
  static func applyFilter(imageFilter: ImageFilter, toImage image: UIImage) -> UIImage {
    if let ciImage = CIImage(image: image) {
      let filter: CIFilter?
      switch imageFilter {
      case .Original:
        filter = nil
      case .ColorCrossPolynomial:
        let redCoeficient = CIVector(values: [1, 0, 0, 0, 0, 0, 0, 0, 0], count: 9)
        let greenCoeficient = CIVector(values: [1, 0, 0, 0, 0, 0, 0, 0, 0], count: 9)
        let blueCoeficient = CIVector(values: [1, 0, 0, 0, 0, 0, 0, 0, 0], count: 9)
        if NSProcessInfo().isOperatingSystemAtLeastVersion(NSOperatingSystemVersion(majorVersion: 8, minorVersion: 0, patchVersion: 0)) {
          let params = [
            kCIInputImageKey: ciImage,
            "inputRedCoefficients": redCoeficient,
            "inputGreenCoefficients": greenCoeficient,
            "inputBlueCoefficients": blueCoeficient]
          filter = CIFilter(name: "CIColorCrossPolynomial", withInputParameters: params)
        } else {
          filter = CIFilter(name: "CIColorCrossPolynomial")
          filter?.setValue(ciImage, forKey: kCIInputImageKey)
          filter?.setValue(redCoeficient, forKey: "inputRedCoefficients")
          filter?.setValue(greenCoeficient, forKey: "inputGreenCoefficients")
          filter?.setValue(blueCoeficient, forKey: "inputBlueCoefficients")
        }
//      case .ColorCube:
//        if NSProcessInfo().isOperatingSystemAtLeastVersion(NSOperatingSystemVersion(majorVersion: 8, minorVersion: 0, patchVersion: 0)) {
//          filter = CIFilter(name: "CISepiaTone", withInputParameters: [kCIInputImageKey: ciImage, "inputIntensity": 0.4])
//        } else {
//          filter = CIFilter(name: "CISepiaTone")
//          filter?.setValue(ciImage, forKey: kCIInputImageKey)
//          filter?.setValue(0.4, forKey: "inputIntensity")
//        }
//      case .ColorCubeColorSpace:
//        if NSProcessInfo().isOperatingSystemAtLeastVersion(NSOperatingSystemVersion(majorVersion: 8, minorVersion: 0, patchVersion: 0)) {
//          filter = CIFilter(name: "CISepiaTone", withInputParameters: [kCIInputImageKey: ciImage, "inputIntensity": 0.4])
//        } else {
//          filter = CIFilter(name: "CISepiaTone")
//          filter?.setValue(ciImage, forKey: kCIInputImageKey)
//          filter?.setValue(0.4, forKey: "inputIntensity")
//        }
      case .ColorInvert:
        if NSProcessInfo().isOperatingSystemAtLeastVersion(NSOperatingSystemVersion(majorVersion: 8, minorVersion: 0, patchVersion: 0)) {
          filter = CIFilter(name: "CIColorInvert", withInputParameters: [kCIInputImageKey: ciImage])
        } else {
          filter = CIFilter(name: "CIColorInvert")
          filter?.setValue(ciImage, forKey: kCIInputImageKey)
        }
//      case .ColorMap:
//        if NSProcessInfo().isOperatingSystemAtLeastVersion(NSOperatingSystemVersion(majorVersion: 8, minorVersion: 0, patchVersion: 0)) {
//          filter = CIFilter(name: "CISepiaTone", withInputParameters: [kCIInputImageKey: ciImage, "inputIntensity": 0.4])
//        } else {
//          filter = CIFilter(name: "CISepiaTone")
//          filter?.setValue(ciImage, forKey: kCIInputImageKey)
//          filter?.setValue(0.4, forKey: "inputIntensity")
//        }
      case .ColorMonochrome:
//        inputColor
//        A CIColor object whose attribute type is CIAttributeTypeOpaqueColor and whose display name is Color.
        let color = CIColor(red: 0.4, green: 0.4, blue: 0.6, alpha: 1.0)
        if NSProcessInfo().isOperatingSystemAtLeastVersion(NSOperatingSystemVersion(majorVersion: 8, minorVersion: 0, patchVersion: 0)) {
          filter = CIFilter(name: "CIColorMonochrome", withInputParameters: [kCIInputImageKey: ciImage, kCIInputIntensityKey: 0.4, kCIInputColorKey: color])
        } else {
          filter = CIFilter(name: "CIColorMonochrome")
          filter?.setValue(ciImage, forKey: kCIInputImageKey)
          filter?.setValue(color, forKey: kCIInputColorKey)
          filter?.setValue(0.4, forKey: kCIInputIntensityKey)
        }
//      case .ColorPosterize:
//        if NSProcessInfo().isOperatingSystemAtLeastVersion(NSOperatingSystemVersion(majorVersion: 8, minorVersion: 0, patchVersion: 0)) {
//          filter = CIFilter(name: "CIColorPosterize", withInputParameters: [kCIInputImageKey: ciImage, "inputLevels": 0.6])
//        } else {
//          filter = CIFilter(name: "CIColorPosterize")
//          filter?.setValue(ciImage, forKey: kCIInputImageKey)
//          filter?.setValue(0.6, forKey: "inputLevels")
//        }
//      case .FalseColor:
//        let color0 = CIColor(red: 0.3, green: 0.5, blue: 0.7, alpha: 1)
//        let color1 = CIColor(red: 0.5, green: 0.5, blue: 0.1, alpha: 1)
//        if NSProcessInfo().isOperatingSystemAtLeastVersion(NSOperatingSystemVersion(majorVersion: 8, minorVersion: 0, patchVersion: 0)) {
//          filter = CIFilter(name: "CIFalseColor", withInputParameters: [kCIInputImageKey: ciImage, "inputColor0": color0, "inputColor1": color1])
//        } else {
//          filter = CIFilter(name: "CIFalseColor")
//          filter?.setValue(ciImage, forKey: kCIInputImageKey)
//          filter?.setValue(color0, forKey: "inputColor0")
//          filter?.setValue(color1, forKey: "inputColor1")
//        }
//      case .MaskToAlpha:
//        if NSProcessInfo().isOperatingSystemAtLeastVersion(NSOperatingSystemVersion(majorVersion: 8, minorVersion: 0, patchVersion: 0)) {
//          filter = CIFilter(name: "CISepiaTone", withInputParameters: [kCIInputImageKey: ciImage, kCIInputIntensityKey: 0.4])
//        } else {
//          filter = CIFilter(name: "CISepiaTone")
//          filter?.setValue(ciImage, forKey: kCIInputImageKey)
//          filter?.setValue(0.4, forKey: kCIInputIntensityKey)
//        }
      case .MaximumComponent:
        if NSProcessInfo().isOperatingSystemAtLeastVersion(NSOperatingSystemVersion(majorVersion: 8, minorVersion: 0, patchVersion: 0)) {
          filter = CIFilter(name: "CIMaximumComponent", withInputParameters: [kCIInputImageKey: ciImage])
        } else {
          filter = CIFilter(name: "CIMaximumComponent")
          filter?.setValue(ciImage, forKey: kCIInputImageKey)
        }
      case .MinimumComponent:
        if NSProcessInfo().isOperatingSystemAtLeastVersion(NSOperatingSystemVersion(majorVersion: 8, minorVersion: 0, patchVersion: 0)) {
          filter = CIFilter(name: "CIMinimumComponent", withInputParameters: [kCIInputImageKey: ciImage])
        } else {
          filter = CIFilter(name: "CIMinimumComponent")
          filter?.setValue(ciImage, forKey: kCIInputImageKey)
        }
      case .PhotoEffectChrome:
        if NSProcessInfo().isOperatingSystemAtLeastVersion(NSOperatingSystemVersion(majorVersion: 8, minorVersion: 0, patchVersion: 0)) {
          filter = CIFilter(name: "CIPhotoEffectChrome", withInputParameters: [kCIInputImageKey: ciImage])
        } else {
          filter = CIFilter(name: "CIPhotoEffectChrome")
          filter?.setValue(ciImage, forKey: kCIInputImageKey)
        }
      case .PhotoEffectFade:
        if NSProcessInfo().isOperatingSystemAtLeastVersion(NSOperatingSystemVersion(majorVersion: 8, minorVersion: 0, patchVersion: 0)) {
          filter = CIFilter(name: "CIPhotoEffectFade", withInputParameters: [kCIInputImageKey: ciImage])
        } else {
          filter = CIFilter(name: "CIPhotoEffectFade")
          filter?.setValue(ciImage, forKey: kCIInputImageKey)
        }
      case .PhotoEffectInstant:
        if NSProcessInfo().isOperatingSystemAtLeastVersion(NSOperatingSystemVersion(majorVersion: 8, minorVersion: 0, patchVersion: 0)) {
          filter = CIFilter(name: "CIPhotoEffectInstant", withInputParameters: [kCIInputImageKey: ciImage])
        } else {
          filter = CIFilter(name: "CIPhotoEffectInstant")
          filter?.setValue(ciImage, forKey: kCIInputImageKey)
        }
      case .PhotoEffectMono:
        if NSProcessInfo().isOperatingSystemAtLeastVersion(NSOperatingSystemVersion(majorVersion: 8, minorVersion: 0, patchVersion: 0)) {
          filter = CIFilter(name: "CIPhotoEffectMono", withInputParameters: [kCIInputImageKey: ciImage])
        } else {
          filter = CIFilter(name: "CIPhotoEffectMono")
          filter?.setValue(ciImage, forKey: kCIInputImageKey)
        }
      case .PhotoEffectNoir:
        if NSProcessInfo().isOperatingSystemAtLeastVersion(NSOperatingSystemVersion(majorVersion: 8, minorVersion: 0, patchVersion: 0)) {
          filter = CIFilter(name: "CIPhotoEffectNoir", withInputParameters: [kCIInputImageKey: ciImage])
        } else {
          filter = CIFilter(name: "CIPhotoEffectNoir")
          filter?.setValue(ciImage, forKey: kCIInputImageKey)
        }
      case .PhotoEffectProcess:
        if NSProcessInfo().isOperatingSystemAtLeastVersion(NSOperatingSystemVersion(majorVersion: 8, minorVersion: 0, patchVersion: 0)) {
          filter = CIFilter(name: "CIPhotoEffectProcess", withInputParameters: [kCIInputImageKey: ciImage])
        } else {
          filter = CIFilter(name: "CIPhotoEffectProcess")
          filter?.setValue(ciImage, forKey: kCIInputImageKey)
        }
      case .PhotoEffectTonal:
        if NSProcessInfo().isOperatingSystemAtLeastVersion(NSOperatingSystemVersion(majorVersion: 8, minorVersion: 0, patchVersion: 0)) {
          filter = CIFilter(name: "CIPhotoEffectTonal", withInputParameters: [kCIInputImageKey: ciImage])
        } else {
          filter = CIFilter(name: "CIPhotoEffectTonal")
          filter?.setValue(ciImage, forKey: kCIInputImageKey)
        }
      case .PhotoEffectTransfer:
        if NSProcessInfo().isOperatingSystemAtLeastVersion(NSOperatingSystemVersion(majorVersion: 8, minorVersion: 0, patchVersion: 0)) {
          filter = CIFilter(name: "CIPhotoEffectTransfer", withInputParameters: [kCIInputImageKey: ciImage])
        } else {
          filter = CIFilter(name: "CIPhotoEffectTransfer")
          filter?.setValue(ciImage, forKey: kCIInputImageKey)
        }
      case .SepiaTone:
        let intensity = 0.4
        if NSProcessInfo().isOperatingSystemAtLeastVersion(NSOperatingSystemVersion(majorVersion: 8, minorVersion: 0, patchVersion: 0)) {
          filter = CIFilter(name: "CISepiaTone", withInputParameters: [kCIInputImageKey: ciImage, kCIInputIntensityKey: intensity])
        } else {
          filter = CIFilter(name: "CISepiaTone")
          filter?.setValue(ciImage, forKey: kCIInputImageKey)
          filter?.setValue(intensity, forKey: kCIInputIntensityKey)
        }
      case .Vignette:
        let intensity = 0.4
        let radius = 1
        if NSProcessInfo().isOperatingSystemAtLeastVersion(NSOperatingSystemVersion(majorVersion: 8, minorVersion: 0, patchVersion: 0)) {
          filter = CIFilter(name: "CIVignette", withInputParameters: [kCIInputImageKey: ciImage, kCIInputIntensityKey: intensity, kCIInputRadiusKey: radius])
        } else {
          filter = CIFilter(name: "CIVignette")
          filter?.setValue(ciImage, forKey: kCIInputImageKey)
          filter?.setValue(intensity, forKey: kCIInputIntensityKey)
          filter?.setValue(radius, forKey: kCIInputRadiusKey)
        }
      case .VignetteEffect:
        let center = CIVector(x: 150, y: 150)
        let radius = 1
        let intensity = 0.4
        if NSProcessInfo().isOperatingSystemAtLeastVersion(NSOperatingSystemVersion(majorVersion: 8, minorVersion: 0, patchVersion: 0)) {
          filter = CIFilter(name: "CIVignetteEffect", withInputParameters: [kCIInputImageKey: ciImage, kCIInputIntensityKey: intensity, kCIInputCenterKey: center, kCIInputRadiusKey: radius])
        } else {
          filter = CIFilter(name: "CISepiaTone")
          filter?.setValue(ciImage, forKey: kCIInputImageKey)
          filter?.setValue(intensity, forKey: kCIInputIntensityKey)
          filter?.setValue(radius, forKey: kCIInputRadiusKey)
          filter?.setValue(center, forKey: kCIInputCenterKey)
        }
      }
      
      if let filter = filter, outputImage = UIImage(CGImage: CIContext(options: nil).createCGImage(filter.outputImage, fromRect: filter.outputImage.extent())) {
        
        return outputImage
      }
    }
    
    return image
  }
  
  func filterImage(image: UIImage) -> UIImage {

    return ImageFilter.applyFilter(self, toImage: image)
  }
  
  func filterImageThumb(image: UIImage) -> UIImage {
    
    let filteredImage = filterImage(image)
    let thumbSize = CGSize(width: 100, height: 100)
    UIGraphicsBeginImageContextWithOptions(thumbSize, false, 0)
    filteredImage.drawInRect(CGRect(origin: CGPoint.zeroPoint, size: thumbSize))
    let thumbImage = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    
    return thumbImage
  }
  
  func title() -> String {
    let title: String
    switch self {
    case .Original:
      title = "Original"
    case .ColorCrossPolynomial:
      title = "Polino"
    case .ColorInvert:
      title = "Negativo"
    case .ColorMonochrome:
      title = "Mono"
    case .MaximumComponent:
      title = "Compomá"
    case .MinimumComponent:
      title = "Compomi"
    case .PhotoEffectChrome:
      title = "Cromado"
    case .PhotoEffectFade:
      title = "Fade"
    case .PhotoEffectInstant:
      title = "Instant"
    case .PhotoEffectMono:
      title = "Mono 2"
    case .PhotoEffectNoir:
      title = "Noir"
    case .PhotoEffectProcess:
      title = "Pro"
    case .PhotoEffectTonal:
      title = "Tonal"
    case .PhotoEffectTransfer:
      title = "Transfer"
    case .SepiaTone:
      title = "Sepia"
    case .Vignette:
      title = "Vignette"
    case .VignetteEffect:
      title = "Vignette 2"
    }
    return title
  }
}

extension UIImage {
  func colorCrossPolynomial() -> UIImage {
    return ImageFilter.applyFilter(.ColorCrossPolynomial, toImage: self)
  }
  
  func colorInvert() -> UIImage {
    return ImageFilter.applyFilter(.ColorInvert, toImage: self)
  }
  
  func colorMonochrome() -> UIImage {
    return ImageFilter.applyFilter(.ColorMonochrome, toImage: self)
  }
  
  func maximumComponent() -> UIImage {
    return ImageFilter.applyFilter(.MaximumComponent, toImage: self)
  }
  
  func minimumComponent() -> UIImage {
    return ImageFilter.applyFilter(.MinimumComponent, toImage: self)
  }
  
  func photoEffectChrome() -> UIImage {
    return ImageFilter.applyFilter(.PhotoEffectChrome, toImage: self)
  }
  
  func photoEffectFade() -> UIImage {
    return ImageFilter.applyFilter(.PhotoEffectFade, toImage: self)
  }
  
  func photoEffectInstant() -> UIImage {
    return ImageFilter.applyFilter(.PhotoEffectInstant, toImage: self)
  }
  
  func photoEffectMono() -> UIImage {
    return ImageFilter.applyFilter(.PhotoEffectMono, toImage: self)
  }
  
  func photoEffectNoir() -> UIImage {
    return ImageFilter.applyFilter(.PhotoEffectNoir, toImage: self)
  }
  
  func photoEffectProcess() -> UIImage {
    return ImageFilter.applyFilter(.PhotoEffectProcess, toImage: self)
  }
  
  func photoEffectTonal() -> UIImage {
    return ImageFilter.applyFilter(.PhotoEffectTonal, toImage: self)
  }
  
  func photoEffectTransfer() -> UIImage {
    return ImageFilter.applyFilter(.PhotoEffectTransfer, toImage: self)
  }
  
  func sepiaImage() -> UIImage {
    return ImageFilter.applyFilter(.SepiaTone, toImage: self)
  }
  
  func vignette() -> UIImage {
    return ImageFilter.applyFilter(.Vignette, toImage: self)
  }
  
  func vignetteEffect() -> UIImage {
    return ImageFilter.applyFilter(.VignetteEffect, toImage: self)
  }
}
