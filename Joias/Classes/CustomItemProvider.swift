//
//  CustomItemProvider.swift
//  Joias
//
//  Created by Gilson Gil on 7/20/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class CustomItemProvider: UIActivityItemProvider {
  let text: String?
  
  init(text: String?) {
    self.text = text
    super.init()
  }
  
  override func activityViewControllerPlaceholderItem(activityViewController: UIActivityViewController) -> AnyObject {
    return text ?? ""
  }
  
  override func activityViewController(activityViewController: UIActivityViewController, itemForActivityType activityType: String) -> AnyObject? {
    if let text = text {
      if activityType == "net.whatsapp.WhatsApp.ShareExtension" {
        return nil
      }
      return text
    }
    return nil
  }
}
