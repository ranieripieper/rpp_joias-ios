//
//  Share.swift
//  Joias
//
//  Created by Gilson Gil on 3/14/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class Share {
  class func jewel(jewel: Jewel?, image: UIImage?, inViewController viewController: UIViewController) {
    var activityItems: [AnyObject] {
      if let image = image {
        return [CustomItemProvider(text: "\(jewel!.name) - \(jewel!.description)"), image]
      } else {
        return ["\(jewel!.name) - \(jewel!.description)"]
      }
    }
    dispatch_async(dispatch_get_main_queue()) {
      let activityViewController = UIActivityViewController(activityItems: activityItems, applicationActivities: nil)
      viewController.presentViewController(activityViewController, animated: true, completion: nil)
    }
  }
  
  class func photo(image: UIImage, jewel: Jewel, inViewController viewController: UIViewController) {
    dispatch_async(dispatch_get_main_queue()) {
      let activityItems = [image, "Saiba mais da \(Constants.appName) em nosso aplicativo exclusivo + \(Constants.appStoreLink)"]
      let activityViewController = UIActivityViewController(activityItems: activityItems, applicationActivities: nil)
      viewController.presentViewController(activityViewController, animated: true, completion: nil)
    }
  }
}
