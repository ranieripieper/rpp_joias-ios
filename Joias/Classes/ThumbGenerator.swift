//
//  ThumbGenerator.swift
//  Joias
//
//  Created by Gilson Gil on 5/11/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit
import AVFoundation

struct ThumbGenerator {
  static func generate(url: String, completion: Result<(UIImage, String)> -> ()) {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0)) {
      let asset = AVURLAsset(URL: NSURL(string: url), options: nil)
      let generator = AVAssetImageGenerator(asset: asset)
      generator.appliesPreferredTrackTransform = true
      let thumbTime = CMTimeMakeWithSeconds(1, 30)
      let width = UIScreen.mainScreen().bounds.width * UIScreen.mainScreen().scale
      let maxSize = CGSize(width: width, height: width * 9 / 16)
      generator.maximumSize = maxSize
      generator.generateCGImagesAsynchronouslyForTimes([NSValue(CMTime: thumbTime)]) { _, image, _, result, error in
        if result == .Succeeded {
          if let image = image {
            let tuple = (UIImage(CGImage: image)!, url)
            completion(Result(tuple))
          } else {
            completion(Result(Error(code: -1, domain: "com.doisdoissete.Joias", userInfo: ["description": "unknown error"])))
          }
        } else {
          completion(Result(Error(code: -1, domain: "com.doisdoissete.Joias", userInfo: ["description": "unknown error"])))
        }
      }
    }
  }
}
