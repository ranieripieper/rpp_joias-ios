//
//  Try.swift
//  Joias
//
//  Created by Gilson Gil on 5/22/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import Foundation

extension Jewel {
  class func tryEarrings() -> [Jewel] {
    var earrings = [Jewel]()
    for i in 1...Constants.earringsCount {
      let jewel = Jewel(name: "brinco_\(i)", picture: Picture(URL: "thumb_brinco_\(i)", width: 0, height: 0), category: .Earrings, collection: .None, description: "")
      earrings.append(jewel)
    }
    
    return earrings
  }
  
  class func tryRings() -> [Jewel] {
    var rings = [Jewel]()
    for i in 1...Constants.ringsCount {
      let jewel = Jewel(name: "anel_\(i)", picture: Picture(URL: "thumb_anel_\(i)", width: 0, height: 0), category: .Ring, collection: .None, description: "")
      rings.append(jewel)
    }
    
    return rings
    }
  
  class func tryNecklaces() -> [Jewel] {
    return []
  }
  
  class func tryBracelets() -> [Jewel] {
    return []
  }
}
