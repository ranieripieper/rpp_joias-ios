//
//  Menu.swift
//  Joias
//
//  Created by Gilson Gil on 3/11/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

struct Menu {
  static var menu: [(String, String)] {
    if ParseHelper.loggedInUser() == nil {
      return [("Home", "icn_menu_home"), ("Coleções", "icn_menu_colecoes"), ("Looks & Press", "icn_menu_looksandpress"), ("Experimente uma Jóia", "icn_menu_experimente"), ("Vídeos", "icn_menu_videos"), ("Facebook", "icn_menu_fb"), ("Instagram", "icn_menu_instagram"), ("Sobre", "icn_menu_sobre"), ("Entrar", "icn_login")]
    } else {
      return [("Home", "icn_menu_home"), ("Coleções", "icn_menu_colecoes"), ("Looks & Press", "icn_menu_looksandpress"), ("Experimente uma Jóia", "icn_menu_experimente"), ("Vídeos", "icn_menu_videos"), ("Facebook", "icn_menu_fb"), ("Instagram", "icn_menu_instagram"), ("Meu Perfil", "icn_user"), ("Sobre", "icn_menu_sobre"), ("Sair", "icn_logout")]
    }
  }
  
  static func count() -> Int {
    return menu.count
  }
  
  static func stringAtIndex(index: Int) -> String {
    return menu[index].0
  }
  
  static func iconAtIndex(index: Int) -> UIImage {
    return UIImage(named: menu[index].1)!
  }
}