//
//  VideoCell.swift
//  Joias
//
//  Created by Gilson Gil on 3/13/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

protocol VideoCellDelegate {
  func playVideoAtCell(videoCell: VideoCell)
}

class VideoCell: UITableViewCell {
  @IBOutlet weak var thumbButton: UIButton!
  @IBOutlet weak var videoLabel: UILabel!
  
  var delegate: VideoCellDelegate?
  
  func configureWithVideo(video: Video) {
    videoLabel.text = video.label
    if let thumb = video.thumb {
      thumbButton.setBackgroundImage(thumb, forState: .Normal)
    } else {
      thumbButton.setBackgroundImage(nil, forState: .Normal)
      ThumbGenerator.generate(video.originalLink) {
        switch $0 {
        case .Success(let boxed):
          dispatch_async(dispatch_get_main_queue()) {
            if boxed.unbox.1 == video.originalLink {
              video.thumb = boxed.unbox.0
              self.thumbButton.setBackgroundImage(boxed.unbox.0, forState: .Normal)
            }
          }
        case .Failure(let error):
          break
        }
      }
    }
  }
  
  @IBAction func play(sender: UIButton) {
    delegate?.playVideoAtCell(self)
  }
}
