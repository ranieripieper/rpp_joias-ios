//
//  CollectionCell.swift
//  Joias
//
//  Created by Gilson Gil on 2/23/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class CollectionCell: UICollectionViewCell {
  @IBOutlet weak var pictureImageView: UIImageView!
  @IBOutlet weak var nameLabel: UILabel!
  @IBOutlet weak var categoryLabel: UILabel!
  
  var pictureRequest: Request?
  var imageCache = NSCache()
  
  override func awakeFromNib() {
    super.awakeFromNib()
    nameLabel.font = UIFont.lightFontWithSize(12)
    categoryLabel.font = UIFont.lightFontWithSize(10)
    categoryLabel.textColor = UIColor.collectionCategoryColor()
  }
  
  func configureWithJewel(jewel: Jewel) {
    nameLabel.text = jewel.name.uppercaseString
    categoryLabel.text = jewel.collection.toString()
    if pictureRequest?.request.URLString != jewel.picture.URL {
      pictureRequest?.cancel()
    }
    
    if let image = self.imageCache.objectForKey(jewel.picture) as? UIImage {
      pictureImageView.image = image
    } else {
      pictureImageView.image = nil
      
      pictureRequest = request(.GET, jewel.picture.URL.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)!)
        .validate(contentType: ["image/*"])
        .responseImage() { req, _, image, error in
          if error == nil && image != nil {
            self.imageCache.setObject(image!, forKey: req.URLString)
            if req.URLString == self.pictureRequest?.request.URLString {
              self.pictureImageView.image = image
            }
          }
      }
    }
  }
}
