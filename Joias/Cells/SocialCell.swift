//
//  SocialCell.swift
//  Joias
//
//  Created by Gilson Gil on 2/22/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class SocialCell: UITableViewCell {
  @IBOutlet weak var usernameButton: UIButton!
  @IBOutlet weak var timeLabel: UILabel!
  @IBOutlet weak var descriptionLabel: UILabel!
  @IBOutlet weak var mediaImageView: UIImageView!
  @IBOutlet weak var likeButton: UIButton!
  @IBOutlet weak var commentButton: UIButton!
  @IBOutlet weak var shareButton: UIButton?
  
  var post: Post?
  var pictureRequest: Request?
  let imageCache = NSCache()
  lazy var tap: UITapGestureRecognizer = {
    let tapGesture = UITapGestureRecognizer()
    tapGesture.numberOfTapsRequired = 2
    tapGesture.addTarget(self, action: "interactWithSocialNetwork:")
    return tapGesture
  }()
  
  override func awakeFromNib() {
    super.awakeFromNib()
    let usernameAttributedTitle = NSAttributedString(string: "", attributes: [NSForegroundColorAttributeName: usernameButton.titleColorForState(.Normal)!, NSFontAttributeName: UIFont.semiboldFontWithSize(14)])
    usernameButton.setAttributedTitle(usernameAttributedTitle, forState: .Normal)
    let likesAttributedTitle = NSAttributedString(string: "", attributes: [NSForegroundColorAttributeName: likeButton.titleColorForState(.Normal)!, NSFontAttributeName: UIFont.mediumFontWithSize(11)])
    likeButton.setAttributedTitle(likesAttributedTitle, forState: .Normal)
    commentButton.setAttributedTitle(likesAttributedTitle, forState: .Normal)
    timeLabel.font = UIFont.mediumFontWithSize(11)
    descriptionLabel.font = UIFont.lightFontWithSize(13)
  }
  
  func configureWithPost(post: Post) {
    self.post = post
    usernameButton.setTitle(post.username, forState: .Normal)
    timeLabel.text = post.timestamp.differenceFrom(NSDate().timeIntervalSince1970)
    descriptionLabel.text = post.description
    likeButton.setTitle("\(post.likes ?? 0)", forState: .Normal)
    commentButton.setTitle("\(post.comments ?? 0)", forState: .Normal)
    shareButton?.setTitle("\(post.shares ?? 0)", forState: .Normal)
    
    // Cancel the request if it's not for the same photo after dequeue
    if pictureRequest?.request.URLString != post.picture {
      pictureRequest?.cancel()
    }
    
    if let image = self.imageCache.objectForKey(post.picture) as? UIImage {
      mediaImageView.image = image
    } else {
      mediaImageView.image = nil
      
      pictureRequest = request(.GET, post.picture)
        .validate(contentType: ["image/*"])
        .responseImage() { req, _, image, error in
          if error == nil && image != nil {
            self.imageCache.setObject(image!, forKey: req.URLString)
            
            // Make sure that by the time this line is executed, the cell is supposed the display the same image with the same URL.
            if req.URLString == self.pictureRequest?.request.URLString {
              self.mediaImageView.image = image
            }
          }
      }
    }
    
    switch post.socialType {
    case .Facebook:
      break
    case .Instagram:
      mediaImageView.addGestureRecognizer(tap)
    }
  }
  
  @IBAction func interactWithSocialNetwork(sender: AnyObject) {
    if let link = post?.link {
      UIApplication.sharedApplication().openURL(NSURL(string: link)!)
    }
  }
}
