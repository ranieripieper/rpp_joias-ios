//
//  JewelDetailCell.swift
//  Joias
//
//  Created by Gilson Gil on 7/22/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class JewelDetailCell: UICollectionViewCell {
  var currentViewController: UIViewController?
  
  func configureWithViewController(viewController: UIViewController) {
    if let currentViewController = currentViewController {
      currentViewController.view.removeFromSuperview()
    }
    viewController.view.frame = contentView.bounds
    contentView.addSubview(viewController.view)
    currentViewController = viewController
  }
}
