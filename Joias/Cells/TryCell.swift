//
//  TryCell.swift
//  Joias
//
//  Created by Gilson Gil on 3/12/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class TryCell: UICollectionViewCell {
  @IBOutlet weak var jewelImageView: UIImageView!
  
  func configureWithImageString(imageString: String) {
    jewelImageView.image = UIImage(named: imageString)
  }
}
