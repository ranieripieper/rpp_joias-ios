//
//  LooksPressCell.swift
//  Joias
//
//  Created by Gilson Gil on 3/14/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class LooksPressCell: UICollectionViewCell {
  @IBOutlet weak var lookImageView: UIImageView!
  
  var pictureRequest: Request?
  var imageCache = NSCache()
  
  func configureWithImage(image: String) {
    if pictureRequest?.request.URLString != image {
      pictureRequest?.cancel()
    }
    
    if let image = self.imageCache.objectForKey(image) as? UIImage {
      lookImageView.image = image
    } else {
      lookImageView.image = nil
      
      pictureRequest = request(.GET, image)
        .validate(contentType: ["image/*"])
        .responseImage() { req, _, picture, error in
          if error == nil && picture != nil {
            self.imageCache.setObject(picture!, forKey: req.URLString)
            if req.URLString == self.pictureRequest?.request.URLString {
              self.lookImageView.image = picture
            }
          }
      }
    }
  }
}
