//
//  MenuCell.swift
//  Joias
//
//  Created by Gilson Gil on 3/11/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class MenuCell: UITableViewCell {
  @IBOutlet weak var iconImageView: UIImageView!
  @IBOutlet weak var label: UILabel!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    label.font = UIFont.lightFontWithSize(18)
  }
  
  func configureWithText(text: String, icon: UIImage) {
    iconImageView.image = icon
    label.text = text
  }
  
  override func setHighlighted(highlighted: Bool, animated: Bool) {
    if highlighted {
      contentView.backgroundColor = UIColor.whiteColor()
    } else {
      contentView.backgroundColor = UIColor.clearColor()
    }
  }
  
  override func setSelected(selected: Bool, animated: Bool) {
    if selected {
      contentView.backgroundColor = UIColor.whiteColor()
    } else {
      contentView.backgroundColor = UIColor.clearColor()
    }
  }
}
