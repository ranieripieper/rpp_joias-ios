//
//  TryFilterCell.swift
//  
//
//  Created by Gilson Gil on 10/13/15.
//
//

import UIKit

class TryFilterCell: UICollectionViewCell {
  @IBOutlet weak var imageView: UIImageView!
  @IBOutlet weak var label: UILabel!
  
  override var selected: Bool {
    didSet {
      if selected {
        layer.borderColor = UIColor.collectionButtonsColor().CGColor
        layer.borderWidth = 8
      } else {
        layer.borderColor = UIColor.clearColor().CGColor
        layer.borderWidth = 0
      }
    }
  }
  
  func configureWithImage(image: UIImage?, text: String) {
    imageView.image = image
    label.text = text
  }
}
