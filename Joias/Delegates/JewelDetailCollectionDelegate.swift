//
//  JewelDetailCollectionDelegate.swift
//  Joias
//
//  Created by Gilson Gil on 7/22/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class JewelDetailCollectionDelegate: NSObject {
   @IBOutlet weak var jewelDetailCollectionViewController: JewelDetailCollectionViewController!
}

extension JewelDetailCollectionDelegate: UICollectionViewDataSource {
  func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return jewelDetailCollectionViewController.jewelDetailViewControllers.count
  }
  
  func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
    let jewelDetailCell = collectionView.dequeueReusableCellWithReuseIdentifier("JewelDetailCell", forIndexPath: indexPath) as! JewelDetailCell
    if jewelDetailCollectionViewController.jewelDetailViewControllers[indexPath.item] == nil, let jewelDetailViewController = jewelDetailCollectionViewController.storyboard?.instantiateViewControllerWithIdentifier("JewelDetailViewController") as? JewelDetailViewController {
      jewelDetailViewController.jewel = jewelDetailCollectionViewController.jewels[indexPath.item]
      jewelDetailCollectionViewController.jewelDetailViewControllers.removeAtIndex(indexPath.item)
      jewelDetailCollectionViewController.jewelDetailViewControllers.insert(jewelDetailViewController, atIndex: indexPath.item)
    }
    if let jewelDetailViewController = jewelDetailCollectionViewController.jewelDetailViewControllers[indexPath.item] {
      jewelDetailCell.configureWithViewController(jewelDetailViewController)
    }
    return jewelDetailCell
  }
}

extension JewelDetailCollectionDelegate: UICollectionViewDelegate {
  
}

extension JewelDetailCollectionDelegate: UICollectionViewDelegateFlowLayout {
  func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
    return CGSize(width: jewelDetailCollectionViewController.view.bounds.width, height: jewelDetailCollectionViewController.view.bounds.height - 64)
  }
}

extension JewelDetailCollectionDelegate: UIScrollViewDelegate {
  func scrollViewDidScroll(scrollView: UIScrollView) {
    if scrollView.dragging {
      let page = Int(scrollView.contentOffset.x / scrollView.bounds.width + 0.5)
      jewelDetailCollectionViewController.currentIndex = page
    }
  }
}
