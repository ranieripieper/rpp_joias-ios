//
//  VideosTableViewDelegate.swift
//  Joias
//
//  Created by Gilson Gil on 3/13/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class VideosTableViewDelegate: NSObject {
  @IBOutlet weak var videosViewController: VideosViewController!
  
  var datasource = [Video]()
}

extension VideosTableViewDelegate: UITableViewDataSource, UITableViewDelegate {
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return datasource.count
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let videoCell = tableView.dequeueReusableCellWithIdentifier("VideoCell", forIndexPath: indexPath) as! VideoCell
    let video = datasource[indexPath.row]
    videoCell.configureWithVideo(video)
    videoCell.delegate = videosViewController
    return videoCell
  }
}

extension VideosTableViewDelegate: UIScrollViewDelegate {
  func scrollViewDidScroll(scrollView: UIScrollView) {
    if scrollView.contentOffset.y + 2 * scrollView.bounds.height > scrollView.contentSize.height {
      videosViewController.getVideos()
    }
  }
}
