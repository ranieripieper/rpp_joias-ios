//
//  HorizontalFlowLayout.swift
//  Joias
//
//  Created by Gilson Gil on 7/23/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class HorizontalFlowLayout: UICollectionViewFlowLayout {
  var centerItemIndex = 0
  
  required init(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    scrollDirection = .Horizontal
    itemSize = CGSize(width: UIScreen.mainScreen().bounds.width, height: UIScreen.mainScreen().bounds.height - 64)
    minimumInteritemSpacing = 0
    minimumLineSpacing = 0
  }
  
  override func layoutAttributesForElementsInRect(rect: CGRect) -> [AnyObject]? {
    let visibleRect = CGRect(origin: collectionView!.contentOffset, size: collectionView!.bounds.size)
    let maxDistance = visibleRect.size.width * 0.5
    
    if let layoutAttributes = super.layoutAttributesForElementsInRect(rect) as? [UICollectionViewLayoutAttributes] {
      for attributes in layoutAttributes {
        let distance = CGRectGetMidX(visibleRect) - attributes.center.x
        if abs(distance) < 1 {
          centerItemIndex = attributes.indexPath.item
        }
        let centerItem = layoutAttributes.filter {
          $0.indexPath.item == self.centerItemIndex
          }.first
        if attributes == centerItem {
          continue
        }
        var normalizedDistance = distance / maxDistance
        normalizedDistance = min(normalizedDistance, 1)
        normalizedDistance = max(normalizedDistance, -1)
        let zoom = 1 - abs(normalizedDistance) * 0.1
        
        var transform = CATransform3DIdentity
        transform = CATransform3DScale(transform, zoom, zoom, 0)
        transform = CATransform3DTranslate(transform, 0, 0, distance / maxDistance)
        attributes.transform3D = transform
      }
      return layoutAttributes
    }
    return nil
  }
  
  override func shouldInvalidateLayoutForBoundsChange(newBounds: CGRect) -> Bool {
    return true
  }
}
