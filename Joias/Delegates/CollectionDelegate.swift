//
//  CollectionDelegate.swift
//  Joias
//
//  Created by Gilson Gil on 2/23/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class CollectionDelegate: NSObject {
  @IBOutlet weak var collectionViewController: CollectionViewController!
  @IBOutlet weak var collectionView: UICollectionView!
  
  var stackedGridLayout: StackedGridLayout?
  var datasource = [Jewel]()
  
  override func awakeFromNib() {
    super.awakeFromNib()
    stackedGridLayout = StackedGridLayout()
    stackedGridLayout!.delegate = self
  }
}

extension CollectionDelegate: UICollectionViewDataSource, UICollectionViewDelegate {
  func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
    return 1
  }
  
  func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return datasource.count
  }
  
  func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
    let collectionCell = collectionView.dequeueReusableCellWithReuseIdentifier("CollectionCell", forIndexPath: indexPath) as! CollectionCell
    collectionCell.configureWithJewel(datasource[indexPath.item])
    return collectionCell
  }
  
  func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
    collectionViewController.goToJewelDetailsAtIndex(indexPath.item)
  }
}

extension CollectionDelegate: StackedGridLayoutDelegate {
  func collectionView(collectionView: UICollectionView, layout: UICollectionViewLayout, numberOfColumnsInSection section: Int) -> Int {
    return 2
  }
  
  func collectionView(collectionView: UICollectionView, layout: UICollectionViewLayout, itemInsetsForSectionAtIndex section: Int) -> UIEdgeInsets {
    return UIEdgeInsets(top: 4, left: 4, bottom: 12, right: 4)
  }
  
  func collectionView(collectionView: UICollectionView, layout: UICollectionViewLayout, sizeForItemWithWidth width: CGFloat, atIndexPath indexPath: NSIndexPath) -> CGSize {
    let jewel = datasource[indexPath.item]
    let picture = jewel.picture
    let width = (UIScreen.mainScreen().bounds.width - 16) / 2.0
    let height = width * CGFloat(jewel.picture.height / jewel.picture.width) + 67
    return CGSize(width: width, height: height)
  }
}

extension CollectionDelegate: UIScrollViewDelegate {
  func scrollViewDidScroll(scrollView: UIScrollView) {
    if scrollView.contentOffset.y + 2 * scrollView.bounds.height > scrollView.contentSize.height {
      collectionViewController.getJewels()
    }
  }
}
