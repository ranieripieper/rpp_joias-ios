//
//  SocialTableViewDelegate.swift
//  Joias
//
//  Created by Gilson Gil on 2/22/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class SocialTableViewDelegate: NSObject {
  @IBOutlet weak var socialViewController: SocialViewController!
  
  var datasource = [Post]()
  var onceToken: dispatch_once_t = 0
  var sizingCell: SocialCell?
}

extension SocialTableViewDelegate: UITableViewDataSource, UITableViewDelegate {
  func numberOfSectionsInTableView(tableView: UITableView) -> Int {
    return 1
  }
  
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return datasource.count
  }
  
  func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    return UITableViewAutomaticDimension
  }
  
  func calculateHeightForConfiguredCell(cell: SocialCell) -> CGFloat {
    cell.setNeedsUpdateConstraints()
    cell.updateConstraintsIfNeeded()
    cell.bounds = CGRect(x: 0.0, y: 0.0, width: UIScreen.mainScreen().bounds.width, height: cell.bounds.height)
    cell.setNeedsLayout()
    cell.layoutIfNeeded()
    let size = cell.contentView.systemLayoutSizeFittingSize(UILayoutFittingCompressedSize)
    return size.height + 1.0
  }
  
  func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
    return 1
  }
  
  func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
    let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.width, height: 1))
    view.backgroundColor = UIColor.clearColor()
    return view
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    var identifier: String {
      if socialViewController.socialType == .Facebook {
        return "FacebookSocialCell"
      } else {
        return "InstagramSocialCell"
      }
    }
    let cell = tableView.dequeueReusableCellWithIdentifier(identifier, forIndexPath: indexPath) as! SocialCell
    let post = datasource[indexPath.row]
    cell.configureWithPost(post)
    return cell
  }
}

extension SocialTableViewDelegate: UIScrollViewDelegate {
  func scrollViewDidScroll(scrollView: UIScrollView) {
    if scrollView.contentOffset.y + 2 * scrollView.bounds.height > scrollView.contentSize.height {
      socialViewController.getMedia()
    }
  }
}