//
//  MenuTableViewDelegate.swift
//  Joias
//
//  Created by Gilson Gil on 3/11/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class MenuTableViewDelegate: NSObject {
  @IBOutlet weak var menuViewController: MenuViewController!
  @IBOutlet weak var tableViewHeightConstraint: NSLayoutConstraint!
}

extension MenuTableViewDelegate: UITableViewDataSource, UITableViewDelegate {
  func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    return 50
  }
  
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    let count = Menu.count()
    tableViewHeightConstraint.constant = min(CGFloat(count * 50), UIScreen.mainScreen().bounds.height - 64)
    return count
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let menuCell = tableView.dequeueReusableCellWithIdentifier("MenuCell", forIndexPath: indexPath) as! MenuCell
    menuCell.configureWithText(Menu.stringAtIndex(indexPath.row), icon: Menu.iconAtIndex(indexPath.row))
    return menuCell
  }
  
  func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    switch indexPath.row {
    case 0:
      menuViewController.goToHome()
    case 1:
      menuViewController.goToCollections()
    case 2:
      menuViewController.goToLooks()
    case 3:
      menuViewController.goToTry()
    case 4:
      menuViewController.goToVideos()
    case 5:
      menuViewController.goToFacebook()
    case 6:
      menuViewController.goToInstagram()
    case 7:
      if ParseHelper.loggedInUser() == nil {
        menuViewController.goToAbout()
      } else {
        menuViewController.goToMyProfile()
      }
    case 8:
      if ParseHelper.loggedInUser() == nil {
        menuViewController.login()
      } else {
        menuViewController.goToAbout()
      }
    case 9:
      menuViewController.logout()
    default:
      break
    }
  }
}
