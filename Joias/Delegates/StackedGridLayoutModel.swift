//
//  StackedGridLayoutModel.swift
//  Joias
//
//  Created by Gilson Gil on 2/23/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class StackedGridLayoutModel: NSObject {
  var frame: CGRect
  let itemInsets: UIEdgeInsets
  let columnWidth: CGFloat
  var numberOfItems: Int {
    return indexToFrameMap.count
  }
  var columnHeights = [CGFloat]()
  var indexToFrameMap = [Int: AnyObject]()
  
  init(origin: CGPoint, width: CGFloat, columns: Int, itemInsets: UIEdgeInsets) {
    frame = CGRect(origin: origin, size: CGSize(width: width, height: 0.0))
    self.itemInsets = itemInsets
    columnWidth = floor(width / CGFloat(columns))
    for var i = 0; i < columns; i++ {
      columnHeights.append(0)
    }
  }
  
  func addItemOfSize(size: CGSize, forIndex index: Int) {
    var shortestColumnHeight = CGFloat.max
    var shortestColumnIndex: Int = 0
    for height in columnHeights {
      let thisColumnHeight = height
      if thisColumnHeight < shortestColumnHeight {
        shortestColumnHeight = thisColumnHeight
        shortestColumnIndex = find(columnHeights, height)!
      }
    }
    let frame = CGRect(x: self.frame.origin.x + columnWidth * CGFloat(shortestColumnIndex) + itemInsets.left, y: self.frame.origin.y + shortestColumnHeight + itemInsets.top, width: size.width, height: size.height)
    indexToFrameMap[index] = NSValue(CGRect: frame)
    
    if CGRectGetMaxY(frame) > CGRectGetMaxY(self.frame) {
      self.frame = CGRect(origin: self.frame.origin, size: CGSize(width: self.frame.width, height: CGRectGetMaxY(frame) - self.frame.origin.y + itemInsets.bottom))
    }
    columnHeights.removeAtIndex(shortestColumnIndex)
    columnHeights.insert(shortestColumnHeight + size.height + itemInsets.bottom, atIndex: shortestColumnIndex)
  }
  
  func frameForItemAtIndex(index: Int) -> CGRect {
    if let value = indexToFrameMap[index] as? NSValue {
      return value.CGRectValue()
    }
    return CGRectZero
  }
}