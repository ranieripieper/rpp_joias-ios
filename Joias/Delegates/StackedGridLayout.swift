//
//  StackedGridLayout.swift
//  Joias
//
//  Created by Gilson Gil on 2/23/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

protocol StackedGridLayoutDelegate: UICollectionViewDataSource {
  func collectionView(collectionView: UICollectionView, layout: UICollectionViewLayout, numberOfColumnsInSection section: Int) -> Int
  func collectionView(collectionView: UICollectionView, layout: UICollectionViewLayout, sizeForItemWithWidth width: CGFloat, atIndexPath indexPath: NSIndexPath) -> CGSize
  func collectionView(collectionView: UICollectionView, layout: UICollectionViewLayout, itemInsetsForSectionAtIndex section: Int) -> UIEdgeInsets
}

class StackedGridLayout: UICollectionViewLayout {
  weak var delegate: StackedGridLayoutDelegate!
  var sectionData = [StackedGridLayoutModel]()
  var height: CGFloat = 0
  
  override func prepareLayout() {
    super.prepareLayout()
    
    if delegate != nil {
      sectionData.removeAll(keepCapacity: false)
      height = 0
      var currentOrigin = CGPointZero
      let numberOfSections = collectionView?.numberOfSections()
      for var i = 0; i < numberOfSections; i++ {
        currentOrigin.y = height
        let numberOfColumns = delegate.collectionView(collectionView!, layout: self, numberOfColumnsInSection: i)
        let numberOfItems = collectionView?.numberOfItemsInSection(i)
        let itemInsets = delegate.collectionView(collectionView!, layout: self, itemInsetsForSectionAtIndex: i)
        let section = StackedGridLayoutModel(origin: currentOrigin, width: UIScreen.mainScreen().bounds.width, columns: numberOfColumns, itemInsets: itemInsets)
        for var j = 0; j < numberOfItems; j++ {
          let itemWidth = section.columnWidth - section.itemInsets.left - section.itemInsets.right
          let itemIndexPath = NSIndexPath(forItem: j, inSection: i)
          let itemSize = delegate.collectionView(collectionView!, layout: self, sizeForItemWithWidth: itemWidth, atIndexPath: itemIndexPath)
          section.addItemOfSize(itemSize, forIndex: j)
        }
        sectionData.append(section)
        height += section.frame.height
        currentOrigin.y = height
      }
    }
  }
  
  override func collectionViewContentSize() -> CGSize {
    return CGSize(width: UIScreen.mainScreen().bounds.width, height: height)
  }

  override func layoutAttributesForItemAtIndexPath(indexPath: NSIndexPath) -> UICollectionViewLayoutAttributes! {
    let section = sectionData[indexPath.section]
    let attributes = UICollectionViewLayoutAttributes(forCellWithIndexPath: indexPath)
    attributes.frame = section.frameForItemAtIndex(indexPath.item)
    return attributes
  }

  override func layoutAttributesForElementsInRect(rect: CGRect) -> [AnyObject]? {
    var attributes = [UICollectionViewLayoutAttributes]()
    for section in sectionData {
      let sectionFrame = section.frame
      if CGRectIntersectsRect(sectionFrame, rect) {
        for var index = 0; index < section.numberOfItems; index++ {
          let frame = section.frameForItemAtIndex(index)
          if CGRectIntersectsRect(frame, rect) {
            let indexPath = NSIndexPath(forItem: index, inSection: find(sectionData, section)!)
            let la = layoutAttributesForItemAtIndexPath(indexPath)
            attributes.append(la)
          }
        }
      }
    }
    return attributes
  }
}
