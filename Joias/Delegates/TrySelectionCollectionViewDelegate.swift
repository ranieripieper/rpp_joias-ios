//
//  TrySelectionCollectionViewDelegate.swift
//  Joias
//
//  Created by Gilson Gil on 3/12/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class TrySelectionCollectionViewDelegate: NSObject {
  @IBOutlet weak var trySelectionViewController: TrySelectionViewController!
  
  var datasource = [Jewel]()
}

extension TrySelectionCollectionViewDelegate: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
  func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
    let width = (UIScreen.mainScreen().bounds.width - 40) / 2
    return CGSize(width: width, height: width * 391 / 466)
  }
  
  func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
    return CGSize(width: collectionView.bounds.width, height: 60)
  }
  
  func collectionView(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, atIndexPath indexPath: NSIndexPath) -> UICollectionReusableView {
    let header = collectionView.dequeueReusableSupplementaryViewOfKind(UICollectionElementKindSectionHeader, withReuseIdentifier: "TrySelectionHeader", forIndexPath: indexPath) as! TrySelectionHeader
    switch trySelectionViewController.jewelCategory! {
    case .Earrings:
      header.configureWithJewelCategoryString("brinco")
    case .Necklace:
      header.configureWithJewelCategoryString("colar")
    case .Ring:
      header.configureWithJewelCategoryString("anel")
    case .Bracelet:
      header.configureWithJewelCategoryString("pulseira")
    case .None:
      break
    }
    return header
  }
  
  func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return datasource.count
  }
  
  func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
    let tryCell = collectionView.dequeueReusableCellWithReuseIdentifier("TryCell", forIndexPath: indexPath) as! TryCell
    tryCell.configureWithImageString(datasource[indexPath.item].picture.URL)
    return tryCell
  }
  
  func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
    trySelectionViewController?.try(datasource[indexPath.item])
  }
}
