//
//  LooksPressCollectionViewDelegate.swift
//  Joias
//
//  Created by Gilson Gil on 3/14/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class LooksPressCollectionViewDelegate: NSObject {
  @IBOutlet weak var looksPressViewController: LooksPressViewController!
  
  var stackedGridLayout: StackedGridLayout?
  var datasource = [Jewel]()
  
  override func awakeFromNib() {
    super.awakeFromNib()
    stackedGridLayout = StackedGridLayout()
    stackedGridLayout!.delegate = self
  }
}

extension LooksPressCollectionViewDelegate: UICollectionViewDataSource, UICollectionViewDelegate {
  func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return datasource.count
  }
  
  func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
    let looksPressCell = collectionView.dequeueReusableCellWithReuseIdentifier("LooksPressCell", forIndexPath: indexPath) as! LooksPressCell
    looksPressCell.configureWithImage(datasource[indexPath.item].picture.URL)
    return looksPressCell
  }
}

extension LooksPressCollectionViewDelegate: StackedGridLayoutDelegate {
  func collectionView(collectionView: UICollectionView, layout: UICollectionViewLayout, numberOfColumnsInSection section: Int) -> Int {
    return 2
  }
  
  func collectionView(collectionView: UICollectionView, layout: UICollectionViewLayout, itemInsetsForSectionAtIndex section: Int) -> UIEdgeInsets {
    return UIEdgeInsetsZero
  }
  
  func collectionView(collectionView: UICollectionView, layout: UICollectionViewLayout, sizeForItemWithWidth width: CGFloat, atIndexPath indexPath: NSIndexPath) -> CGSize {
    let jewel = datasource[indexPath.item]
    let picture = jewel.picture
    let width = UIScreen.mainScreen().bounds.width / 2.0
    let height = width * CGFloat(jewel.picture.height / jewel.picture.width)
    return CGSize(width: width, height: height)
  }
}

extension LooksPressCollectionViewDelegate: UIScrollViewDelegate {
  func scrollViewDidScroll(scrollView: UIScrollView) {
    if scrollView.contentOffset.y + 2 * scrollView.bounds.height > scrollView.contentSize.height {
      looksPressViewController.getLooksPress()
    }
  }
}
