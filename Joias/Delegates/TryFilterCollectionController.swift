//
//  TryFilterCollectionController.swift
//  
//
//  Created by Gilson Gil on 10/13/15.
//
//

import UIKit

protocol TryFilterCollectionControllerDelegate {
  func didSelectFilterAtIndex(index: Int)
}

class TryFilterCollectionController: NSObject {
  var datasource: [(UIImage, String)] = []
  var delegate: TryFilterCollectionControllerDelegate?
}

extension TryFilterCollectionController: UICollectionViewDataSource {
  func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return datasource.count
  }
  
  func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCellWithReuseIdentifier("TryFilterCell", forIndexPath: indexPath) as! TryFilterCell
    let object = datasource[indexPath.item]
    cell.configureWithImage(object.0, text: object.1)

    return cell
  }
}

extension TryFilterCollectionController: UICollectionViewDelegate {
  func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
    delegate?.didSelectFilterAtIndex(indexPath.item)
  }
}

extension TryFilterCollectionController: UICollectionViewDelegateFlowLayout {
  func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
    
    return UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
  }
  
  func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
    
    return 20
  }
  
  func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
    
    return 20
  }
  
  func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {

    return CGSize(width: collectionView.bounds.height, height: collectionView.bounds.height)
  }
}
